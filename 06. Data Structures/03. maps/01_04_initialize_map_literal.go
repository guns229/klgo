package main

import "fmt"

func main() {
	pincode_locality := map[int]string{
		462003: "SOUTH T.T. NAGAR P.O.",
		465680: "Naahli B.O",
		465691: "Dhatarawda B.O",
		465697: "Bhainswamataji B.O",
		466651: "Khajuriyakalan B.O",
	}

	for key, value := range pincode_locality {
		fmt.Println("key:", key, "and value:", value)
	}
}
