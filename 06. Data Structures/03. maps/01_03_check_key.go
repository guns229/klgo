package main

import (
	"fmt"
)

func main() {
	var pincode_locality map[int]string
	pincode_locality = make(map[int]string)

	pincode_locality[462003] = "SOUTH T.T. NAGAR B.O."
	pincode_locality[465680] = "Naahli B.O"
	pincode_locality[465697] = "Biaora Mandu B.O"
	pincode_locality[466651] = "Muktarnagar B.O"
	val, flag := pincode_locality[465697]
	fmt.Println(val, flag)
	delete(pincode_locality, 465697)
	val, flag = pincode_locality[465697]
	fmt.Println(val, flag)
}
