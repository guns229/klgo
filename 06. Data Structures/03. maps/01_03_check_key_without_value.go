package main

import (
	"fmt"
)

func main() {
	var pincode_locality map[int]string
	pincode_locality = make(map[int]string)

	pincode_locality[462003] = "SOUTH T.T. NAGAR B.O."
	pincode_locality[465680] = "Naahli B.O"
	pincode_locality[465697] = "Biaora Mandu B.O"
	pincode_locality[466651] = "Muktarnagar B.O"
	_, b := pincode_locality[465697]
	fmt.Println(b)
	delete(pincode_locality, 465697)
	_, b = pincode_locality[465697]
	fmt.Println(b)
}
