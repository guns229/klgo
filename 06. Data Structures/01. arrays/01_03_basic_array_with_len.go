package main

import (
    "fmt"
    "math/rand"
    "time"
)

func main() {
    var nums [10]int

    rand.Seed(time.Now().UnixNano())
    for x := 0; x < len(nums); x++ {
        nums[x] = rand.Intn(111)
    }
    for x := 0; x < len(nums); x++ {
        fmt.Print(nums[x], ", ")
    }
}
