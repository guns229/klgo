package main

import "fmt"

func main() {

	nums := []int{
		36, 31, 64,
		45, 2, 1, 12, 6, 7, 38, 18,
		6, 7, 22, 21,
	}

	fmt.Println("Original array:", nums)
	slice_1 := nums[2:6]
	fmt.Println("Original Slice", slice_1)
	slice_1 = append(slice_1, 4, 5, 17)
	fmt.Println("updated slice", slice_1)
	fmt.Println("Original array after append in slice:", nums)
}
