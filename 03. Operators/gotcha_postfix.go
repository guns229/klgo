package main

import "fmt"

func main() {
  
  var (
    a = 10
    b = 10.3
    c =  3.141592653589793238
  )
  
  // The below will not work.

  fmt.Println(a++)
  fmt.Println(b--)
  fmt.Println(c++)
}
