package main

import "fmt"

func main() {
	A := 'a'
	B := 'z'

	fmt.Println("A + B: ", A+B)
	fmt.Println("A - B: ", A-B)
	fmt.Println("A * B: ", A*B)
	fmt.Println("A / B: ", A/B)
	fmt.Println("A % B: ", A%B)
	A++
	fmt.Println("A++  : ", A)
	A--
	fmt.Println("A--  : ", A)
}
