package main

import "fmt"

func main() {
	A := 10
	B := 12

	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
	fmt.Println("A > B: ", A > B)
	fmt.Println("A < B: ", A < B)
	fmt.Println("A >= B: ", A >= B)
	fmt.Println("A <= B: ", A <= B)
}
