package main

import "fmt"

func main() {
	A := true  // int
	B := false // float

	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
}
