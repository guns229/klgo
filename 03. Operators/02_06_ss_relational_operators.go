package main

import "fmt"

func main() {
	A := "Mayank Johri"
	B := "mayank johri" 

	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
	fmt.Println("A > B: ", A > B)
	fmt.Println("A < B: ", A < B)
	fmt.Println("A >= B: ", A >= B)
	fmt.Println("A <= B: ", A <= B)
}
