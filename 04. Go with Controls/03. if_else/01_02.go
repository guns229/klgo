package main

import "fmt"

func main() {

	var train_number_down = 12156
	var train_number_up = 12156
	// var train_direction = "down"

	if train_direction := "down"; train_direction == "up" {
		fmt.Println("The train is going to Agra and its number is ",
			train_number_up)
	} else {
		fmt.Println("The train is going to Bhopal and its number is ",
			train_number_down)
	}
}
