package main

import "fmt"

func get_digit_numbers(i int) int {
    var x = 0
    for i >= 1 {
        i = i / 10
        x++
    }
    return x
}

func first_number(i int) int {
    for i >= 1 {
        i = i / 10
    }
    return i
}

func last_digit(i int) int {
    return (i % 10)
}

func convert(i int) {
    switch i {
    case 0:
        fmt.Print("null")
    case 1:
        fmt.Print("eins")
    case 2:
        fmt.Print("zwei")
    case 3:
        fmt.Print("drei")
    case 4:
        fmt.Print("vier")
    case 5:
        fmt.Print("fünf")
    default:
        fmt.Println("Unknown Number")
    }
    fmt.Print(" ")
}

func main() {
    var i = 543210

    for i > 0 {
        convert(first_number(i))
        i = i / 10
    }

}
