package main

import "fmt"

func main() {

    var val = "Mayank Johri"

    switch interface{}(val).(type) {
    case bool:
        fmt.Println("boolean")
    case int, int8, int16, int32, int64:
        fmt.Println("int")
    case string:
        fmt.Println("String")
    default:
        fmt.Println("Sorry not processing this data type")
    }
}
