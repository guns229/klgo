package main

import "fmt"

func main() {
    for {
        var provided string
        fmt.Print("\nPlease enter the password: ")
        fmt.Scanln(&provided)
        if provided == "xyz" {
            break
        }
        fmt.Println("Found some issue with password")
    }
}
