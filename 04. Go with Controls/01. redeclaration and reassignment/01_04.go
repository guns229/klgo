package main

import "fmt"

func main() {
    a := 0
    b := "Maynak"
    {
        fmt.Println(a, b) // 0 Maynak

        // Equivalent
        // var a, b = 1, "Mayank Johri"
        a, b := 1, "Mayank Johri"
        fmt.Println(a, b) // 1 1
    }
    fmt.Println(a, b) // 1 1
}
