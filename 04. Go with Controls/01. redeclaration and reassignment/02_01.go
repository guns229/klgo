package main

import (
    "fmt"
)

func square(x int) (int, string) {
    if x > 100 {
        fmt.Println("Good")
        return 404, "value more than 100"
    }
    fmt.Println("x * x", x*x)
    return x * x, ""
}

func main() {
    var x int
    if x, err := square(1); len(err) > 0 {
        fmt.Println("!!! Error !!!\n", x, err)
        return
    }
    fmt.Println("x: ", x)
    if x, err := square(10); len(err) > 0 {
        fmt.Println("!!! Error !!!\n", x, err)
        return
    }
    fmt.Println("x: ", x)
    if x, err := square(100); len(err) > 0 {
        fmt.Println("!!! Error !!!\n", x, err)
        return
    }
    fmt.Println("x: ", x)
    if x, err := square(200); len(err) > 0 {
        fmt.Println("!!! Error !!!\n", x, err)
        return
    }
    fmt.Println("x: ", x)
}
