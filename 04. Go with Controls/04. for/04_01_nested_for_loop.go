package main

import "fmt"

func main() {
    var subject = -1
    for day := 1; day < 6; day++ {
        fmt.Println("\nDay ", day)
        subject++
        for period := 1; period < 9; period++ {
            subject++
            fmt.Print(period, subject, "\t\t")
            if subject >= 8 {
                subject = 0
            }
        }
    }
}
