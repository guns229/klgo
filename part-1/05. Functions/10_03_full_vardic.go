package main

import "fmt"

func AddAll(vals ...int) int{
  a, b, c, d := 12, 23, 1, 4 // different defaults values 
  switch len(vals) {
  case 1:
    a = vals[0]
  case 2:
    a = vals[0]
    b = vals[1]
  case 3:
    a = vals[0]
    b = vals[1]
    c = vals[2]
  case 4:
    a = vals[0]
    b = vals[1]
    c = vals[2]
    d = vals[3]
  }
  return a + b + c + d
}

func main() {
	fmt.Println(AddAll(1, 2, 4, 2))
	fmt.Println(AddAll(1 ))
	fmt.Println(AddAll(4, 2))
}
