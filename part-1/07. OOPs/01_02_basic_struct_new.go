package main

import "fmt"

func main() {
	type PointInSpaceAndTime struct {
		x, y, z int64
		t       float64
	}

	p := new(PointInSpaceAndTime)
	fmt.Println(p.x, p.y, p.z, p.t)
	p.x, p.y, p.z, p.t = 10, 100, 201, 22.11
	fmt.Println(p.x, p.y, p.z, p.t)
}
