package main

import (
	"fmt"
	"strings"
)

// Parent 1
type Users struct {
	Name string
	Id   int32
}

func (u Users) SetName(name string) {
	fmt.Println("In Users SetName")
	u.Name = strings.TrimSpace(name)
}

// Parent 2
type Technical struct {
	TechName string
	Rating   int8
}

func (t Technical) SetTechName(techName string) {
	t.TechName = techName
}

// Child
type TechManagers struct {
	Users
	Reportee bool
}

func (u TechManagers) SetName(name string) {
	fmt.Println("In Managers SetName")
	u.Name = strings.TrimSpace(name)
}

func main() {
	// mayank := Managers{"User": {"Name": "Mayank", "Id": 1001},
	// 	"Reportee": true,
	// }
	var mayank TechManagers
	mayank.Id = 10001
	mayank.Name = "Testing"
	mayank.Reportee = true
	fmt.Println(mayank)
	mayank.SetName("MJ")
	fmt.Println(mayank)
}
