package main

import "fmt"

type PointInSpaceAndTime struct {
	x, y, z int64
	t       float64
}

func (p PointInSpaceAndTime) GetTimeInMin() float64 {
	return p.t / 60.0 // magic of `.0`
}

func (p PointInSpaceAndTime) SetTime(time float64) {
	p.t = time
}

func main() {

	p := PointInSpaceAndTime{
		2, 10, 101, 2233.23,
	}

	q := PointInSpaceAndTime{
		2, 10, 101, 2233.23,
	}

	p.SetTime(600.0)

	fmt.Println("p.t:", p.GetTimeInMin())
	fmt.Println("q.t:", q.GetTimeInMin())
}
