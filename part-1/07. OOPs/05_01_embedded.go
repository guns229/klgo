package main

import "fmt"

// Parent
type Users struct {
	Name string
	Id   int32
}

// Child
type Managers struct {
	Users
	Reportee bool
}

func main() {
	// mayank := Managers{"User": {"Name": "Mayank", "Id": 1001},
	// 	"Reportee": true,
	// }
	var mayank Managers
	mayank.Id = 10001
	mayank.Name = "Testing"
	mayank.Reportee = true
	fmt.Println(mayank)
}
