package main

import "fmt"

type Cordinates struct {
	X int
	Y int
}

func (c Cordinates) Display() {
	fmt.Println(c.X, c.Y)
}

func (c Cordinates) Update(x int, y int) {
	c.X = x
	c.Y = y
}

func (c *Cordinates) UpdateCord(x int, y int) {
	c.X = x
	c.Y = y
}

func main() {
	center := Cordinates{1, 2}
	center.Display()
	center.Update(22, 33)
	center.Display()
	center.UpdateCord(222, 333)
	center.Display()
}
