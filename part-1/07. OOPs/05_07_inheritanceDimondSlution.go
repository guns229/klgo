package main

import (
	"fmt"
)

type Users struct {
	id int
}

type Managers struct {
	Users
	reportee bool
}

type Technical struct {
	Users
	techName string
}

type TechManagers struct {
	// This solves the issue
	id int
	Managers
	Technical
}

func main() {
	var tm = new(TechManagers)
	tm.id = 1001

	fmt.Println(tm)
}
