package main

import "regexp"
import "fmt"

// we have an interface books, which can used by
// school books, novels, etc
type Books interface {
	GetWordCount() int
	GetAuthor() string
}

type School_Book struct {
	author string
	pages  string
}

func get_words_from(text string) []string {
	words := regexp.MustCompile("\\w+")
	return words.FindAllString(text, -1)
}

func (sb School_Book) GetWordCount() int {
	wc := get_words_from(sb.pages)
	return len(wc)
}

func (sb School_Book) GetAuthor() string {
	return fmt.Sprintf("Author is %s", sb.author)
}

func details(i Books) {
	fmt.Printf("(%v, %T)\n", i, i)
}

func main() {
	var trig_part_1 = School_Book{
		"Mayank Johri.",
		"This is my first book on Trigonometry, dot dot dot",
	}

	fmt.Println(trig_part_1.GetWordCount())
	fmt.Println(trig_part_1.GetAuthor())
	details(trig_part_1)
	var trig_part_2 = School_Book{}
	details(trig_part_2)
	trig_part_2.author = "Mayank"
	details(trig_part_2)
}
