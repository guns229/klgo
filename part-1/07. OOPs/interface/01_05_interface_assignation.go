package main

import "fmt"

type I1 interface {
	M1()
}
type I2 interface {
	M1()
}
type T struct{}

func (T) M1() {}

func main() {
	var v1 I1 = T{}
	var v2 I2 = v1
	fmt.Println(v2, v1)
	v1.M1()
	v2.M1()
	fmt.Println(v2, v1)
}
