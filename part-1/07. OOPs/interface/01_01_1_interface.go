package main

import "fmt"

type Shops interface {
	Name() string
	SetName(name string)
}

type CoffeeShop struct {
	name string
}

func (cs *CoffeeShop) Name() string {
	return cs.name
}

/*
func (cs *CoffeeShop) SetName(name string) {
	cs.name = name
}
*/
func main() {
	var cs Shops
	cs = &CoffeeShop{"Raj"}
	fmt.Println(cs.Name())
	cs.SetName("Rakesh")
	fmt.Println(cs.Name())
}
