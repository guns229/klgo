/*
This example will show how we can have type with n numbers of
types
*/
package main

import "fmt"

type Circle struct {
	Radius float64
	Area   float64
}

var PI = 3.14
var count = 0

func NewCircle(radius float64) Circle {
	var circle Circle
	if count < 2 {
		area := PI * radius * radius
		circle = Circle{radius, area}
		count++
	}
	return circle
}

func main() {
	a := NewCircle(10)
	fmt.Println(a)

	b := NewCircle(100)
	fmt.Println(b)

	c := NewCircle(10)
	fmt.Println(c)

	d := NewCircle(100)
	fmt.Println(d)

}
