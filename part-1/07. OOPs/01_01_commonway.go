package main

import "fmt"

type User struct {
	Name string
	Id   int
}

func main() {
	// Creating empty object
	mayank := User{}
	fmt.Println(mayank)

	// Creating object with partial Data
	rajesh := User{Name: "Rajesh"}
	fmt.Println(rajesh)

	// Creating object with partial Data
	manish := User{
		Id: 1007,
	}
	fmt.Println(manish)

	// Creating object with full data
	wei := User{
		"Wei Wu", 1001,
	}
	fmt.Println(wei)
}
