package main

import (
	"fmt"
	"log"
	"os"
)

// Common Error handling
// Very crude way to handle errors.
func errorHandle(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	f, err := os.Open("dummy.dummy")
	errorHandle(err)
	fmt.Println(f)
}
