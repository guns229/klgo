package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	f, err := os.Open("dummy.dummy")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Done:", f)
}
