package main

import "fmt"

func TestPanic() error {
	panic("This is Panic")
}

func main() {
	var err error
	defer func() {
		fmt.Println(err)
		if err != nil {
			fmt.Println("Recovered from panic:", err)
		}
	}()

	err = TestPanic()
	// This will never run.
	fmt.Println(err)
}
