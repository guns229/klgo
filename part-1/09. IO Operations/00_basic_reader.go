package main

// This code will not work
import (
	"fmt"
	"io"
	"strings"
)

func main() {
	r := strings.NewReader("!!! Guten Morgan !!!")

	b := make([]byte, 6)

	for count, err := range r.Read(b) {
		if err == io.EOF {
			break
		}
		fmt.Printf("count = %v err = %v b = %v\n", count, err, b)
		fmt.Printf("b[:count] = %q\n", b[:count])
	}
}
