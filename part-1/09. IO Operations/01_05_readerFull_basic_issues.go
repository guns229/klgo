package main

import (
	"fmt"
	"io"
	"strings"
)

func main() {
	// nepali for traveling on train

	byt_size := []int{23, 24, 25}
	for _, x := range byt_size {
		r := strings.NewReader("Welcome to city of lakes")
		fmt.Println(x)
		buf := make([]byte, x)
		for {
			if _, err := io.ReadFull(r, buf); err == io.EOF || err != nil {
				break
			} else {
				fmt.Printf("b = %q\n", buf)
			}
		}
		buf = nil
	}
}
