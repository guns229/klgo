package main

import (
	"io"
	"log"
	"os"
)

func check(err error) {
	if err != nil && err != io.EOF {
		if os.IsNotExist(err) {
			log.Fatal("File does not exist.")
		}

		// panic(err)
	}

}

func main() {
	name := "testfile_temp.txt"
	fi, err := os.OpenFile(name, os.O_RDONLY|os.O_CREATE, 0764)
	check(err)

	err = fi.Close()
	check(err)
	_, err = os.Stat(name)
	check(err)
	_, err = os.Stat("not_existing_file.txt")
	check(err)

}
