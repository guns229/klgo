package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strings"
)

func IOCopy(reader io.Reader) (map[string]interface{}, error) {
	var (
		jsonMap map[string]interface{}
		jsonBuf bytes.Buffer
		_, _    = io.Copy(&jsonBuf, reader)
	)

	return jsonMap, json.Unmarshal(jsonBuf.Bytes(), &jsonMap)
}

func main() {
	var jsonData = `{"data" : [
	{
		"color": "red",
		"value": "#f00"
	},
	{
		"color": "green",
		"value": "#0f0"
	},
	{
		"color": "blue",
		"value": "#00f"
	},
	{
		"color": "cyan",
		"value": "#0ff"
	},
	{
		"color": "magenta",
		"value": "#f0f"
	},
	{
		"color": "yellow",
		"value": "#ff0"
	},
	{
		"color": "black",
		"value": "#000"
	}
]}`
	var jsonStr = strings.NewReader(jsonData)
	fmt.Println(IOCopy(jsonStr))
}
