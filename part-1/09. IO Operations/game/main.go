package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"time"
)

type Question struct {
	Question     string   `json:"question"`
	Propositions []string `json:"propositions"`
	Response     string   `json:"response"`
	Anecdote     string   `json:"anecdote"`
}

func intInSlice(a int, list []int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func getRandomNumbers(maxNum int, count int) []int {
	nums := make([]int, count)
	rand.Seed(time.Now().UnixNano())
	i := 0
	for {
		randNo := rand.Intn(maxNum)
		fmt.Println(randNo, nums)
		if intInSlice(randNo, nums) {
			continue
		}
		nums[i] = randNo
		i++
		if i >= count {
			break
		}
	}
	return nums
}

func main() {
	var quests []Question

	file, _ := ioutil.ReadFile("data/quizz-beginner.json")
	_ = json.Unmarshal([]byte(file), &quests)
	selectedQuests := getRandomNumbers(len(quests), 5)
	for index := range selectedQuests {
		num := selectedQuests[index]
		quest := quests[num]
		fmt.Println("Question:", quest.Question)
		fmt.Println("")
		for i, val := range quest.Propositions {
			fmt.Println(i+1, ":", val)
		}
		var response int
		fmt.Print("Please Enter your choice: ")
		fmt.Scanf("%d", &response)
		result := "Sorry, better luck next time"
		if quest.Propositions[response-1] == quest.Response {
			result = "Good Job"
		}
		fmt.Println("\n\n", result, ", the answer was", quest.Response, "\n")
	}
}
