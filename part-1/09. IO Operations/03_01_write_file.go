package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func check(err error) {
	if err != nil && err != io.EOF {
		if os.IsNotExist(err) {
			log.Fatal("File does not exist.")
		}
		panic(err)
	}

}

func main() {

	f, err := os.Create("write_file.txt")
	check(err)

	defer f.Close()

	byte_name := []byte{83, 104, 121, 97, 109, 32, 83, 104, 97, 114, 109, 97}
	n2, err := f.Write(byte_name)
	check(err)
	fmt.Printf("wrote %d bytes\n", n2)
}
