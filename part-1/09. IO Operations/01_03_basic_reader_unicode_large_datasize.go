package main

import (
	"fmt"
	"io"
	"strings"
)

func main() {
	r := strings.NewReader("शुभ प्रभात !!!")

	b := make([]byte, 32)
	for {
		count, err := r.Read(b)
		if err == io.EOF {
			break
		}
		fmt.Printf("count = %v err = %v b = %v\n", count, err, b)
		fmt.Printf("b[:count] = %q\n", b[:count])
	}
}
