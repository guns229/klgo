package main

import "fmt"

type StreamReader struct{}

func (r StreamReader) Read(b []byte) (int, error) {
	return copy(b, "Ohm"), nil
}

func main() {
	a := StreamReader{}
	buf := make([]byte, 3)
	for i := 0; i < 10; i++ {
		a.Read(buf)
		fmt.Println(buf)
	}
}
