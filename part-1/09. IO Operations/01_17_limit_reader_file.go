package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		// panic(err)
	}
}

func main() {
	// open input file
	fi, err := os.Open("prayers.txt")
	check(err)

	// close `fi` on exit
	defer func() {
		err := fi.Close()
		check(err)
	}()
	var size int64
	size = 20
	// make a buffer to keep chunks that are read
	buf := make([]byte, size)
	buf, err = ioutil.ReadAll(io.LimitReader(fi, size))
	check(err)
	fmt.Printf("%s", buf)
}
