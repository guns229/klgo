package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	s := "1231"
	// string to int
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println(s, i)
}
