package main

import (
	"fmt"
	"strings"
)

func main() {
	msg := `oṃ bhūr bhuvaḥ svaḥ
    tat savitur vareṇyaṃ
    bhargo devasya dhīmahi
    dhiyo yo naḥ prachodayāt`
	for _, word := range strings.SplitN(msg, "a", 2) {
		//word = strings.TrimSpace(word)
		if word != "" {
			fmt.Println(">", word, "<")
		}
	}
}
