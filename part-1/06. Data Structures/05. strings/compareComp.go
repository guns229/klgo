package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Compare("India", "India"))
	fmt.Println(strings.Compare("tEST", "Test"))
	fmt.Println(strings.Compare("Bhopal", "Chennai"))
}
