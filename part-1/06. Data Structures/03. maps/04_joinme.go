package main

import "fmt"

func main() {
	usersA := map[int]string{
		1001: "MJ",
		1002: "MJ 2",
		1003: "MJ 3",
		1004: "MJ 4",
	}

	usersB := map[int]string{
		1001: "MJ 1",
		1005: "MJ 5",
		1006: "MJ 6",
		1007: "MJ 7",
	}
	fmt.Println(usersA)
	fmt.Println(usersB)

	for k, v := range usersB {
		usersA[k] = v
	}
	fmt.Println(usersA)
	fmt.Println(usersB)

}
