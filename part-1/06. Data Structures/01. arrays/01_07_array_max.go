package main

import (
	"fmt"
)

func main() {
  // Populating the first element with 10 and remaining with 0
	nums := [4]int{10}

	fmt.Println("Lets read the values", nums)
	fmt.Println(cap(nums), len(nums))
 
  {
    // Initializing the partial values 
	  nums := [4]int{10, 5}
  	fmt.Println("Lets read the values", nums)
  }

	num_new := [...]int64{100, 101, 102}
	fmt.Println(num_new)
}
