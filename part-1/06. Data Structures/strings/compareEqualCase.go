package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.EqualFold("India", "india"))
	fmt.Println(strings.EqualFold("India", "India"))
	fmt.Println(strings.EqualFold("tEST", "Test"))
	fmt.Println(strings.EqualFold("Bhopal", "Chennai"))
}
