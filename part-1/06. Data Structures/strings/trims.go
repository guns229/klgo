package main

import (
	"fmt"
	"strings"
)

func main() {
	msg := `~~~    oṃ bhūr bhuvaḥ svaḥ
    tat savitur vareṇyaṃ
    bhargo devasya dhīmahi
    dhiyo yo naḥ prachodayāt     ~~~`
	fmt.Println(">", strings.Trim(msg, "~"), "<")
	fmt.Println(">", strings.TrimLeft(msg, "~"), "<")
	fmt.Println(">", strings.TrimRight(msg, "~"), "<")

	trimmer := func(c rune) bool {
		return string(c) == "~" || string(c) == " "
	}

	fmt.Println(">", strings.TrimFunc(msg, trimmer), "<")
	fmt.Println(">", strings.TrimLeftFunc(msg, trimmer), "<")
	fmt.Println(">", strings.TrimRightFunc(msg, trimmer), "<")

}
