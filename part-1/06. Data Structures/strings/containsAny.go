package main

import (
	"fmt"
	"strings"
)

func main() {
	var msg = "This is Indian Ocean"

	fmt.Println(strings.ContainsAny(msg, "his"))
	fmt.Println(strings.ContainsAny(msg, "Is"))
	fmt.Println(strings.ContainsAny(msg, "HA"))
}
