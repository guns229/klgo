package main

import (
	"fmt"
	"strings"
)

func main() {
	SirPauly := "Mr. K.V. Pauly"
	mayank := "Mayank"

	fmt.Println(strings.HasPrefix(SirPauly, "Mr."))
	fmt.Println(strings.HasPrefix(SirPauly, "mr."))
	fmt.Println(strings.HasPrefix(mayank, "Mr."))
}
