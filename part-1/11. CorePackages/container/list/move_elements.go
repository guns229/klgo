package main

import (
	"container/list"
	"fmt"
)

func displayList(lst *list.List) {
	fmt.Println("~~~~~~~~~~~~~~~~~~")
	// Iterate through list and print its contents.
	for e := lst.Front(); e != nil; e = e.Next() {
		fmt.Println(e.Value)
	}
}
func main() {
	// Step 1: Create a new lisl
	lst := list.New()
	/* Step 2: Populating the list */
	e3 := lst.PushFront("Three")
	e2 := lst.PushFront("Two")
	e1 := lst.PushFront("One")
	e0 := lst.PushFront("Zero")
	displayList(lst)
	lst.MoveAfter(e1, e2)
	displayList(lst)
	lst.MoveBefore(e0, e3)
	displayList(lst)
	lst.MoveToFront(e0)
	lst.MoveToBack(e3)
	displayList(lst)

}
