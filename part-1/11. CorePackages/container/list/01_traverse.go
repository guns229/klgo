package main

import (
	"container/list"
	"fmt"
)

func main() {
	// Step 1: Create a new list
	lst := list.New()
	/* Step 2: Populating the list */
	//
	e4 := lst.PushBack("Welcome")
	e1 := lst.PushFront("One")
	lst.InsertBefore("Testing", e4)
	lst.InsertAfter("Slow", e1)

	// Iterate through list and print its contents.
	for e := lst.Front(); e != nil; e = e.Next() {
		fmt.Println(e.Value)
	}
}
