package main

import (
	"errors"
	"fmt"
)

type someAtomicError struct{}

func (e *someAtomicError) Error() string { return "Hi!" }
func checkAtomicError() {
	e := &someAtomicError{}
	e2 := fmt.Errorf("whoa!: %w", e)
	e2IsE := errors.Is(e2, &someAtomicError{})
	fmt.Println("atomic error trace ---\t\t", e2, "\t\t--- is traceable: ", e2IsE)
}

type someWrapperError struct {
	Msg string
	Err error
}

func (e someWrapperError) Error() string {
	return fmt.Sprintf("%s: %v", e.Msg, e.Err)
}
func (e someWrapperError) Unwrap() error { return e.Err }
func checkWrapperError() {
	e := someWrapperError{"Hi!", nil}
	e2 := fmt.Errorf("whoa!: %w", e)
	e2IsE := errors.Is(e2, someWrapperError{"Hi!", nil})
	fmt.Println("wrapper error trace ---\t\t", e2, "\t--- is traceable: ", e2IsE)
}

type somePointerWrapperError struct {
	Msg string
	Err error
}

func (e *somePointerWrapperError) Error() string {
	return fmt.Sprintf("%s: %v", e.Msg, e.Err)
}
func (e *somePointerWrapperError) Unwrap() error { return e.Err }
func checkPointerWrapperError() {
	e := &somePointerWrapperError{"Hi!", nil}
	e2 := fmt.Errorf("whoa!: %w", e)

	var ev *somePointerWrapperError
	e2IsE := errors.As(e2, &ev)
	fmt.Println("pointer wrapper error trace ---\t", e2, "\t--- is traceable: ", e2IsE)
	fmt.Printf("ev = %#v\n", ev)
}

func main() {
	checkAtomicError()
	checkWrapperError()
	checkPointerWrapperError()
}
