package main

import (
	"flag"
	"fmt"
	"strings"
)

type Color string

const (
	ColorBlack  Color = "\u001b[30m"
	ColorRed    Color = "\u001b[31m"
	ColorGreen        = "\u001b[32m"
	ColorYellow       = "\u001b[33m"
	ColorBlue         = "\u001b[34m"
	ColorReset        = "\u001b[0m"
)

func colorize(color Color, message string) {
	fmt.Println(string(color), message, string(ColorReset))
}

func main() {
	color := flag.String("color", "green", "Output Color")
	flag.Parse()
	var col Color
	switch strings.ToLower(*color) {
	case "black":
		col = ColorBlack
	case "blue":
		col = ColorBlue
	case "green":
		col = ColorGreen
	default:
		fmt.Println("Using ColorReset")
		col = ColorReset
	}
	colorize(col, "Welcome to wonderful world of Go !")
}
