package main

import (
	"fmt"
	"path"
)

func main() {
	fmt.Println(path.Ext("/a/b/c/bar.css"))
	fmt.Println(path.Ext("/"))
	fmt.Println(path.Ext(""))
	fmt.Println(path.Ext("c://test.com/test.log/realplayer.exe"))
	fmt.Println(path.Ext("http://www.google.com"))
}
