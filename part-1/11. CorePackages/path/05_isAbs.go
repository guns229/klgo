package main

import (
	"fmt"
	"path"
)

func main() {
	fmt.Println(path.IsAbs("/dev/null"))
	fmt.Println(path.IsAbs("dev/null"))
	fmt.Println(path.IsAbs("http://www.mj.com/null"))
	fmt.Println(path.IsAbs("c://www.mj.com/null"))

}
