package main

import (
	"fmt"
	"path"
)

func main() {
	fmt.Println(path.Dir("/etc/x11/test"))
	fmt.Println(path.Dir("etc/init.d/var.sh"))
	fmt.Println(path.Dir("/home/"))
	fmt.Println(path.Dir("/home"))
	fmt.Println(path.Dir("http://www.mj.com/testPath"))
	fmt.Println(path.Dir("/"))
	fmt.Println(path.Dir(""))
}
