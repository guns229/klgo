package main

import (
	"fmt"
	"path"
)

func main() {
	paths := []string{
		"etc/test",
		"/etc//init",
		"var/log/.",
		"home/mayank/go/..",
		"/../go/keepGoing",
		"/../go/KeepGoing/.././.././pathExamples",
		"http://www.mj.com/go/KeepGoing/.././.././pathExamples",
		"",
	}

	for _, p := range paths {
		fmt.Printf("Clean(%q) = %q\n", p, path.Clean(p))
	}
}
