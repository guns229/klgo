package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Lets sleep for 2 seconds")
	time.Sleep(200 * time.Millisecond)
	fmt.Println("Good Morning")
}
