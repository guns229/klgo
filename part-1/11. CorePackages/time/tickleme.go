package main

import (
	"fmt"
	"math/rand"
	"time"
)

func statusUpdate() string {
	val := fmt.Sprintln(rand.Int())
	return val
}

func main() {
	rand.Seed(time.Now().UnixNano())
	fmt.Println("Lets print random data every 2 seconds")
	c := time.Tick(2 * time.Second)
	for next := range c {
		fmt.Printf("%v %s\n", next, statusUpdate())
	}
}
