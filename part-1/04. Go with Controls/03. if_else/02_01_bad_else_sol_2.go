package main

import "fmt"

func get_train_number(train_direction string) int {
	var train_number_up = 12156
	// default value set
	var result = 12155

	if train_direction == "up" {
		result = train_number_up
	}
	return result

}

func main() {
	fmt.Println(get_train_number("up"))
}
