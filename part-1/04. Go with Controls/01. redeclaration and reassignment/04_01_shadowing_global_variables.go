package main

import "fmt"

//global variable
var i = "שלום"  // शांति

func main() {
    j := "השמש"
    fmt.Println(i, j)
    //Shadowing global var
    i := "सूर्य नमस्कार" //Hello sun
    fmt.Println(i, j)
    fun(i, j)
    surya_namaskar(j)
}

func fun(i, j string) {
    fmt.Println(i, j)
}

func surya_namaskar(j string) {
    fmt.Println(i, j)
}
