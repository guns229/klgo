/*
	In this example we are initializing the value of x using `x := 10`
	before we run the for loop.
*/

package main

import "fmt"

func main() {

	if x := 10; x >= 10 {
		fmt.Println("!!! Hurry !!!", x)
	}

	// Gotcha
	fmt.Println(x)

}
