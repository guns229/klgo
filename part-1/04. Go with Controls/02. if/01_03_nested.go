package main

import "fmt"

func main() {
	var x = 11

	if x >= 10 {
		fmt.Println("Number is greater than or equal to 10")
		if x > 10 {
			fmt.Println("Number is greater than 10")
			return
		}
		fmt.Println("Number is 10")
	}
}
