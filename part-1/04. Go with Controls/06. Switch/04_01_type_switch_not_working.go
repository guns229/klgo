package main

import "fmt"

func main() {
    var x = 10

    switch x.(type) {
    case bool:
        fmt.Println("boolean")
    case int, int8, int16, int32, int64:
        fmt.Println("int")

    }
}
