package main

import "fmt"

/*
func get_digit_numbers(i int) int {
    var x = 0
    for i >= 1 {
        i = i / 10
        x++
    }
    return x
}

func first_number(i int) int {
    for i >= 1 {
        i = i / 10
    }
    return i
}

func last_digit(i int) int {
    return (i % 10)
}

*/

func main() {
	var i = 3
	switch i {
	case 0:
		fmt.Println("null")
	case 1:
		fmt.Println("eins")
	case 2:
		fmt.Println("zwei")
	case 3:
		fmt.Println("drei")
	case 4:
		fmt.Println("vier")
	case 5:
		fmt.Println("fünf")
	default:
		fmt.Println("Unknown Number")
	}
}
