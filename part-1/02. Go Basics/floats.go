package main

import "fmt"

func main() {
  var (
    pi_small = 3.14
    pi_medium float32 = 3.141592653589
    pi_default = 3.141592653589
  )
  fmt.Println(pi_small, pi_medium, pi_default)
}
