package main

import "fmt"

func main() {
	// Prints vim-go
	fmt.Println("vim-go")
	x := 10 // Default Radius
	/*
		fmt.Println("x:",x)
	*/
	fmt.Println("Radius:", x)
	x = x + 1 // adding one to x
	x = x + 1 // adding one to compensate the wear and tear

	fmt.Println("Radius:", x)

}
