package main

import "fmt"

func main() {
	fmt.Println("Hello, नमस्ते, आप कैसे हैं?")
	var hello string
	space := " "
	hello = "Hello"
	var world = "World"
	fmt.Println(hello, space, world)
	fmt.Println(hello, world)
}
