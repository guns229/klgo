package main

import "fmt"

func main() {
	var ohm rune = 'ॐ'
	var chrOhm uint32 = 'ॐ'
	var iOhm uint32 = 2384
	fmt.Println(ohm, chrOhm, iOhm)
	fmt.Printf("%c %c %c\n", ohm, chrOhm, iOhm)
}
