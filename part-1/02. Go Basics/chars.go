package main

import "fmt"

func main() {
	var (
		ohm       rune = 'ॐ'
		copyright byte = '©'
	)

	fmt.Printf("%c is %U and %c is %d\n", ohm, ohm, copyright, copyright)

	fmt.Println("%c is %U and %c is %d\n", ohm, ohm, copyright, copyright)

}
