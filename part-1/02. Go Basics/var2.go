package main

import "fmt"

func main() {
	var val = "String"
	testingString := "Testing with " + val

	fmt.Println(testingString)

	one := 1
	fmt.Println("Sum of 1 + 1 = ", one+one)

	var twoThree int

	fmt.Println("Multiplication of 2 & 23 = ", 2*twoThree)
	twoThree = 23

	fmt.Println("Multiplication of 2 & 23 = ", 2*twoThree)
}
