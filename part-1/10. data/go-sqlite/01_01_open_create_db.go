package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	os.Remove("./faqs.db")

	db, err := sql.Open("sqlite3", "./faqs.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	fmt.Println(db)
}
