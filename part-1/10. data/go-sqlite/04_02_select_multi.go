package main

import (
	"database/sql"
	"fmt"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

const DB_FILE = "db/faqs_multi_import.db"

func check_err(msg string, err error) {
	if err != nil {
		panic(msg + ": " + err.Error())
	}
}

func check_count(rows *sql.Row) (count int) {
	err := rows.Scan(&count)
	check_err("check count", err)
	return count
}

func BulkInsertSubjects(db *sql.DB, sql string, unsavedRows []string) error {
	valueStrings := make([]string, 0, len(unsavedRows))
	valueArgs := make([]interface{}, 0, len(unsavedRows))
	for _, subject := range unsavedRows {
		valueStrings = append(valueStrings, "(?)")
		valueArgs = append(valueArgs, subject)
	}

	stmt := fmt.Sprintf(sql, strings.Join(valueStrings, ","))
	fmt.Println(stmt, valueArgs)
	_, err := db.Exec(stmt, valueArgs...)
	return err
}

func main() {
	db, err := sql.Open("sqlite3", DB_FILE)
	check_err("sql.Open", err)

	defer db.Close()
	db.Exec("PRAGMA journal_mode=WAL;")

	sqlStmt := `SELECT COUNT(*) FROM 
                sqlite_master WHERE type='table' 
                AND name='subjects';`
	row := db.QueryRow(sqlStmt)

	if check_count(row) == 0 {
		sqlStmt = `create table subjects 
                    (id integer not null primary key,
                    name text NOT NULL UNIQUE);`
		_, err = db.Exec(sqlStmt)
		check_err("while creating table subjects", err)
	}

	subjects := []string{
		"Maths",
		"Physics",
		"Chemistry",
		"Computers",
	}

	sql_insert_subjects := "insert into subjects(name) values %s"
	err = BulkInsertSubjects(db, sql_insert_subjects, subjects)
	fmt.Println(err)
	check_err("BulkInsertSubjects", err)
	rows, err := db.Query("select name from subjects;")
	check_err("Query", err)
	defer rows.Close()
	for rows.Next() {
		var name string
		err := rows.Scan(&name)
		check_err("scan row", err)
		fmt.Printf("%s\n", name)
	}
}
