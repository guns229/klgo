package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

const DB_FILE = "db/faqs.db"

func main() {

	db, err := sql.Open("sqlite3", DB_FILE)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	sqlStmt := `DROP TABLE "subjects"`
	_, err = db.Exec(sqlStmt)

	if err != nil {
		switch err.Error() {
		case "no such table: subjects":
			sqlStmt = `create table subjects (id integer not null primary key, name text);`
			_, err = db.Exec(sqlStmt)
			if err != nil {
				log.Printf("%q: %s\n", err, sqlStmt)
				return
			}
		default:
			fmt.Println()
			log.Printf("%q: %s\n", err, sqlStmt)
		}
	}
}
