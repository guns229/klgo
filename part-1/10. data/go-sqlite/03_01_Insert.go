package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

const DB_FILE = "db/faqs.db"

func check_err(err error) {
	if err != nil {
		panic(err)
	}
}

func check_count(rows *sql.Row) (count int) {
	err := rows.Scan(&count)
	check_err(err)
	return count
}

func main() {
	db, err := sql.Open("sqlite3", DB_FILE)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	sqlStmt := `SELECT COUNT(*) FROM 
					sqlite_master WHERE type='table' 
					AND name='subjects';`
	rows := db.QueryRow(sqlStmt)

	if check_count(rows) == 0 {
		sqlStmt = `create table subjects 
						(id integer not null primary key, 
							name text);`
		_, err = db.Exec(sqlStmt)
		check_err(err)
	}

	tx, err := db.Begin()
	check_err(err)

	stmt, err := tx.Prepare("insert into subjects(name) values(?)")
	check_err(err)

	defer stmt.Close()
	subjects := []string{
		"Maths",
		"Physics",
		"Chemistry",
		"Computers",
	}

	for _, subs := range subjects {
		_, err = stmt.Exec(fmt.Sprintf("%s", subs))
		if err != nil {
			log.Fatal(err)
		}
	}
	tx.Commit()
}
