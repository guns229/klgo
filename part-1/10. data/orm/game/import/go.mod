module import/v2

go 1.17

require (
	gorm.io/driver/sqlite v1.2.6
	gorm.io/gorm v1.22.5
)

require (
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
)
