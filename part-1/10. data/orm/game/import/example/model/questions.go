package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

/*
DB Table Details
-------------------------------------


CREATE TABLE `questions` (`id` integer,`created_at` datetime,`updated_at` datetime,`deleted_at` datetime,`question` text,`response` text,`anecdote` text,PRIMARY KEY (`id`))

JSON Sample
-------------------------------------
{    "id": 44,    "created_at": "2171-09-27T23:17:37.371830361+05:30",    "updated_at": "2201-11-12T16:23:37.35333331+05:30",    "deleted_at": "2068-10-27T11:51:40.131974866+05:30",    "question": "BKlkqloNMWLfAEpsZVgSMvqcS",    "response": "PPgkabqkMlMslihTQFTrZLxcX",    "anecdote": "AIoBZPeuEGloaiZNRcBevNwlB"}



*/

// Questions struct is a row record of the questions table in the main database
type Questions struct {
	//[ 0] id                                             integer              null: false  primary: true   isArray: false  auto: false  col: integer         len: -1      default: []
	ID int32 `gorm:"primary_key;column:id;type:integer;" json:"id"`
	//[ 1] created_at                                     datetime             null: true   primary: false  isArray: false  auto: false  col: datetime        len: -1      default: []
	CreatedAt null.Time `gorm:"column:created_at;type:datetime;" json:"created_at"`
	//[ 2] updated_at                                     datetime             null: true   primary: false  isArray: false  auto: false  col: datetime        len: -1      default: []
	UpdatedAt null.Time `gorm:"column:updated_at;type:datetime;" json:"updated_at"`
	//[ 3] deleted_at                                     datetime             null: true   primary: false  isArray: false  auto: false  col: datetime        len: -1      default: []
	DeletedAt null.Time `gorm:"column:deleted_at;type:datetime;" json:"deleted_at"`
	//[ 4] question                                       text                 null: true   primary: false  isArray: false  auto: false  col: text            len: -1      default: []
	Question null.String `gorm:"column:question;type:text;" json:"question"`
	//[ 5] response                                       text                 null: true   primary: false  isArray: false  auto: false  col: text            len: -1      default: []
	Response null.String `gorm:"column:response;type:text;" json:"response"`
	//[ 6] anecdote                                       text                 null: true   primary: false  isArray: false  auto: false  col: text            len: -1      default: []
	Anecdote null.String `gorm:"column:anecdote;type:text;" json:"anecdote"`
}

var questionsTableInfo = &TableInfo{
	Name: "questions",
	Columns: []*ColumnInfo{

		&ColumnInfo{
			Index:              0,
			Name:               "id",
			Comment:            ``,
			Notes:              ``,
			Nullable:           false,
			DatabaseTypeName:   "integer",
			DatabaseTypePretty: "integer",
			IsPrimaryKey:       true,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "integer",
			ColumnLength:       -1,
			GoFieldName:        "ID",
			GoFieldType:        "int32",
			JSONFieldName:      "id",
			ProtobufFieldName:  "id",
			ProtobufType:       "int32",
			ProtobufPos:        1,
		},

		&ColumnInfo{
			Index:              1,
			Name:               "created_at",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "datetime",
			DatabaseTypePretty: "datetime",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "datetime",
			ColumnLength:       -1,
			GoFieldName:        "CreatedAt",
			GoFieldType:        "null.Time",
			JSONFieldName:      "created_at",
			ProtobufFieldName:  "created_at",
			ProtobufType:       "google.protobuf.Timestamp",
			ProtobufPos:        2,
		},

		&ColumnInfo{
			Index:              2,
			Name:               "updated_at",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "datetime",
			DatabaseTypePretty: "datetime",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "datetime",
			ColumnLength:       -1,
			GoFieldName:        "UpdatedAt",
			GoFieldType:        "null.Time",
			JSONFieldName:      "updated_at",
			ProtobufFieldName:  "updated_at",
			ProtobufType:       "google.protobuf.Timestamp",
			ProtobufPos:        3,
		},

		&ColumnInfo{
			Index:              3,
			Name:               "deleted_at",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "datetime",
			DatabaseTypePretty: "datetime",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "datetime",
			ColumnLength:       -1,
			GoFieldName:        "DeletedAt",
			GoFieldType:        "null.Time",
			JSONFieldName:      "deleted_at",
			ProtobufFieldName:  "deleted_at",
			ProtobufType:       "google.protobuf.Timestamp",
			ProtobufPos:        4,
		},

		&ColumnInfo{
			Index:              4,
			Name:               "question",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "text",
			DatabaseTypePretty: "text",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "text",
			ColumnLength:       -1,
			GoFieldName:        "Question",
			GoFieldType:        "null.String",
			JSONFieldName:      "question",
			ProtobufFieldName:  "question",
			ProtobufType:       "string",
			ProtobufPos:        5,
		},

		&ColumnInfo{
			Index:              5,
			Name:               "response",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "text",
			DatabaseTypePretty: "text",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "text",
			ColumnLength:       -1,
			GoFieldName:        "Response",
			GoFieldType:        "null.String",
			JSONFieldName:      "response",
			ProtobufFieldName:  "response",
			ProtobufType:       "string",
			ProtobufPos:        6,
		},

		&ColumnInfo{
			Index:              6,
			Name:               "anecdote",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "text",
			DatabaseTypePretty: "text",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "text",
			ColumnLength:       -1,
			GoFieldName:        "Anecdote",
			GoFieldType:        "null.String",
			JSONFieldName:      "anecdote",
			ProtobufFieldName:  "anecdote",
			ProtobufType:       "string",
			ProtobufPos:        7,
		},
	},
}

// TableName sets the insert table name for this struct type
func (q *Questions) TableName() string {
	return "questions"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (q *Questions) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (q *Questions) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (q *Questions) Validate(action Action) error {
	return nil
}

// TableInfo return table meta data
func (q *Questions) TableInfo() *TableInfo {
	return questionsTableInfo
}
