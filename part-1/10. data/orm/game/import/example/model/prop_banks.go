package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

/*
DB Table Details
-------------------------------------


CREATE TABLE prop_banks (id integer, created_at datetime, updated_at datetime, deleted_at datetime, question_id integer REFERENCES questions (id) ON DELETE CASCADE ON UPDATE CASCADE, prop TEXT NOT NULL, PRIMARY KEY (id))

JSON Sample
-------------------------------------
{    "id": 96,    "created_at": "2074-12-07T22:17:56.884672095+05:30",    "updated_at": "2299-07-12T16:30:30.041913056+05:30",    "deleted_at": "2117-03-04T23:14:27.020206408+05:30",    "question_id": 28,    "prop": "ropTKFPLwJfQrsqDRKumcBFEd"}



*/

// PropBanks struct is a row record of the prop_banks table in the main database
type PropBanks struct {
	//[ 0] id                                             integer              null: false  primary: true   isArray: false  auto: false  col: integer         len: -1      default: []
	ID int32 `gorm:"primary_key;column:id;type:integer;" json:"id"`
	//[ 1] created_at                                     datetime             null: true   primary: false  isArray: false  auto: false  col: datetime        len: -1      default: []
	CreatedAt null.Time `gorm:"column:created_at;type:datetime;" json:"created_at"`
	//[ 2] updated_at                                     datetime             null: true   primary: false  isArray: false  auto: false  col: datetime        len: -1      default: []
	UpdatedAt null.Time `gorm:"column:updated_at;type:datetime;" json:"updated_at"`
	//[ 3] deleted_at                                     datetime             null: true   primary: false  isArray: false  auto: false  col: datetime        len: -1      default: []
	DeletedAt null.Time `gorm:"column:deleted_at;type:datetime;" json:"deleted_at"`
	//[ 4] question_id                                    integer              null: true   primary: false  isArray: false  auto: false  col: integer         len: -1      default: []
	QuestionID null.Int `gorm:"column:question_id;type:integer;" json:"question_id"`
	//[ 5] prop                                           text                 null: false  primary: false  isArray: false  auto: false  col: text            len: -1      default: []
	Prop string `gorm:"column:prop;type:text;" json:"prop"`
}

var prop_banksTableInfo = &TableInfo{
	Name: "prop_banks",
	Columns: []*ColumnInfo{

		&ColumnInfo{
			Index:              0,
			Name:               "id",
			Comment:            ``,
			Notes:              ``,
			Nullable:           false,
			DatabaseTypeName:   "integer",
			DatabaseTypePretty: "integer",
			IsPrimaryKey:       true,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "integer",
			ColumnLength:       -1,
			GoFieldName:        "ID",
			GoFieldType:        "int32",
			JSONFieldName:      "id",
			ProtobufFieldName:  "id",
			ProtobufType:       "int32",
			ProtobufPos:        1,
		},

		&ColumnInfo{
			Index:              1,
			Name:               "created_at",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "datetime",
			DatabaseTypePretty: "datetime",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "datetime",
			ColumnLength:       -1,
			GoFieldName:        "CreatedAt",
			GoFieldType:        "null.Time",
			JSONFieldName:      "created_at",
			ProtobufFieldName:  "created_at",
			ProtobufType:       "google.protobuf.Timestamp",
			ProtobufPos:        2,
		},

		&ColumnInfo{
			Index:              2,
			Name:               "updated_at",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "datetime",
			DatabaseTypePretty: "datetime",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "datetime",
			ColumnLength:       -1,
			GoFieldName:        "UpdatedAt",
			GoFieldType:        "null.Time",
			JSONFieldName:      "updated_at",
			ProtobufFieldName:  "updated_at",
			ProtobufType:       "google.protobuf.Timestamp",
			ProtobufPos:        3,
		},

		&ColumnInfo{
			Index:              3,
			Name:               "deleted_at",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "datetime",
			DatabaseTypePretty: "datetime",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "datetime",
			ColumnLength:       -1,
			GoFieldName:        "DeletedAt",
			GoFieldType:        "null.Time",
			JSONFieldName:      "deleted_at",
			ProtobufFieldName:  "deleted_at",
			ProtobufType:       "google.protobuf.Timestamp",
			ProtobufPos:        4,
		},

		&ColumnInfo{
			Index:              4,
			Name:               "question_id",
			Comment:            ``,
			Notes:              ``,
			Nullable:           true,
			DatabaseTypeName:   "integer",
			DatabaseTypePretty: "integer",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "integer",
			ColumnLength:       -1,
			GoFieldName:        "QuestionID",
			GoFieldType:        "null.Int",
			JSONFieldName:      "question_id",
			ProtobufFieldName:  "question_id",
			ProtobufType:       "int32",
			ProtobufPos:        5,
		},

		&ColumnInfo{
			Index:              5,
			Name:               "prop",
			Comment:            ``,
			Notes:              ``,
			Nullable:           false,
			DatabaseTypeName:   "text",
			DatabaseTypePretty: "text",
			IsPrimaryKey:       false,
			IsAutoIncrement:    false,
			IsArray:            false,
			ColumnType:         "text",
			ColumnLength:       -1,
			GoFieldName:        "Prop",
			GoFieldType:        "string",
			JSONFieldName:      "prop",
			ProtobufFieldName:  "prop",
			ProtobufType:       "string",
			ProtobufPos:        6,
		},
	},
}

// TableName sets the insert table name for this struct type
func (p *PropBanks) TableName() string {
	return "prop_banks"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (p *PropBanks) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (p *PropBanks) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (p *PropBanks) Validate(action Action) error {
	return nil
}

// TableInfo return table meta data
func (p *PropBanks) TableInfo() *TableInfo {
	return prop_banksTableInfo
}
