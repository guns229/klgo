package api

import (
	"net/http"

	"example.com/rest/example/dao"
	"example.com/rest/example/model"

	"github.com/gin-gonic/gin"
	"github.com/guregu/null"
	"github.com/julienschmidt/httprouter"
)

var (
	_ = null.Bool{}
)

func configQuestionsRouter(router *httprouter.Router) {
	router.GET("/questions", GetAllQuestions)
	router.POST("/questions", AddQuestions)
	router.GET("/questions/:argID", GetQuestions)
	router.PUT("/questions/:argID", UpdateQuestions)
	router.DELETE("/questions/:argID", DeleteQuestions)
}

func configGinQuestionsRouter(router gin.IRoutes) {
	router.GET("/questions", ConverHttprouterToGin(GetAllQuestions))
	router.POST("/questions", ConverHttprouterToGin(AddQuestions))
	router.GET("/questions/:argID", ConverHttprouterToGin(GetQuestions))
	router.PUT("/questions/:argID", ConverHttprouterToGin(UpdateQuestions))
	router.DELETE("/questions/:argID", ConverHttprouterToGin(DeleteQuestions))
}

// GetAllQuestions is a function to get a slice of record(s) from questions table in the main database
// @Summary Get list of Questions
// @Tags Questions
// @Description GetAllQuestions is a handler to get a slice of record(s) from questions table in the main database
// @Accept  json
// @Produce  json
// @Param   page     query    int     false        "page requested (defaults to 0)"
// @Param   pagesize query    int     false        "number of records in a page  (defaults to 20)"
// @Param   order    query    string  false        "db sort order column"
// @Success 200 {object} api.PagedResults{data=[]model.Questions}
// @Failure 400 {object} api.HTTPError
// @Failure 404 {object} api.HTTPError
// @Router /questions [get]
// http "http://localhost:8080/questions?page=0&pagesize=20" X-Api-User:user123
func GetAllQuestions(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)
	page, err := readInt(r, "page", 0)
	if err != nil || page < 0 {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	pagesize, err := readInt(r, "pagesize", 20)
	if err != nil || pagesize <= 0 {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	order := r.FormValue("order")

	if err := ValidateRequest(ctx, r, "questions", model.RetrieveMany); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	records, totalRows, err := dao.GetAllQuestions(ctx, page, pagesize, order)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	result := &PagedResults{Page: page, PageSize: pagesize, Data: records, TotalRecords: totalRows}
	writeJSON(ctx, w, result)
}

// GetQuestions is a function to get a single record from the questions table in the main database
// @Summary Get record from table Questions by  argID
// @Tags Questions
// @ID argID
// @Description GetQuestions is a function to get a single record from the questions table in the main database
// @Accept  json
// @Produce  json
// @Param  argID path int true "id"
// @Success 200 {object} model.Questions
// @Failure 400 {object} api.HTTPError
// @Failure 404 {object} api.HTTPError "ErrNotFound, db record for id not found - returns NotFound HTTP 404 not found error"
// @Router /questions/{argID} [get]
// http "http://localhost:8080/questions/1" X-Api-User:user123
func GetQuestions(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)

	argID, err := parseInt32(ps, "argID")
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	if err := ValidateRequest(ctx, r, "questions", model.RetrieveOne); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	record, err := dao.GetQuestions(ctx, argID)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	writeJSON(ctx, w, record)
}

// AddQuestions add to add a single record to questions table in the main database
// @Summary Add an record to questions table
// @Description add to add a single record to questions table in the main database
// @Tags Questions
// @Accept  json
// @Produce  json
// @Param Questions body model.Questions true "Add Questions"
// @Success 200 {object} model.Questions
// @Failure 400 {object} api.HTTPError
// @Failure 404 {object} api.HTTPError
// @Router /questions [post]
// echo '{"id": 44,"created_at": "2171-09-27T23:17:37.371830361+05:30","updated_at": "2201-11-12T16:23:37.35333331+05:30","deleted_at": "2068-10-27T11:51:40.131974866+05:30","question": "BKlkqloNMWLfAEpsZVgSMvqcS","response": "PPgkabqkMlMslihTQFTrZLxcX","anecdote": "AIoBZPeuEGloaiZNRcBevNwlB"}' | http POST "http://localhost:8080/questions" X-Api-User:user123
func AddQuestions(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)
	questions := &model.Questions{}

	if err := readJSON(r, questions); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	if err := questions.BeforeSave(); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
	}

	questions.Prepare()

	if err := questions.Validate(model.Create); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	if err := ValidateRequest(ctx, r, "questions", model.Create); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	var err error
	questions, _, err = dao.AddQuestions(ctx, questions)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	writeJSON(ctx, w, questions)
}

// UpdateQuestions Update a single record from questions table in the main database
// @Summary Update an record in table questions
// @Description Update a single record from questions table in the main database
// @Tags Questions
// @Accept  json
// @Produce  json
// @Param  argID path int true "id"
// @Param  Questions body model.Questions true "Update Questions record"
// @Success 200 {object} model.Questions
// @Failure 400 {object} api.HTTPError
// @Failure 404 {object} api.HTTPError
// @Router /questions/{argID} [put]
// echo '{"id": 44,"created_at": "2171-09-27T23:17:37.371830361+05:30","updated_at": "2201-11-12T16:23:37.35333331+05:30","deleted_at": "2068-10-27T11:51:40.131974866+05:30","question": "BKlkqloNMWLfAEpsZVgSMvqcS","response": "PPgkabqkMlMslihTQFTrZLxcX","anecdote": "AIoBZPeuEGloaiZNRcBevNwlB"}' | http PUT "http://localhost:8080/questions/1"  X-Api-User:user123
func UpdateQuestions(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)

	argID, err := parseInt32(ps, "argID")
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	questions := &model.Questions{}
	if err := readJSON(r, questions); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	if err := questions.BeforeSave(); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
	}

	questions.Prepare()

	if err := questions.Validate(model.Update); err != nil {
		returnError(ctx, w, r, dao.ErrBadParams)
		return
	}

	if err := ValidateRequest(ctx, r, "questions", model.Update); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	questions, _, err = dao.UpdateQuestions(ctx,
		argID,
		questions)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	writeJSON(ctx, w, questions)
}

// DeleteQuestions Delete a single record from questions table in the main database
// @Summary Delete a record from questions
// @Description Delete a single record from questions table in the main database
// @Tags Questions
// @Accept  json
// @Produce  json
// @Param  argID path int true "id"
// @Success 204 {object} model.Questions
// @Failure 400 {object} api.HTTPError
// @Failure 500 {object} api.HTTPError
// @Router /questions/{argID} [delete]
// http DELETE "http://localhost:8080/questions/1" X-Api-User:user123
func DeleteQuestions(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx := initializeContext(r)

	argID, err := parseInt32(ps, "argID")
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	if err := ValidateRequest(ctx, r, "questions", model.Delete); err != nil {
		returnError(ctx, w, r, err)
		return
	}

	rowsAffected, err := dao.DeleteQuestions(ctx, argID)
	if err != nil {
		returnError(ctx, w, r, err)
		return
	}

	writeRowsAffected(w, rowsAffected)
}
