package main

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type Question struct {
	gorm.Model
	Question     string     `json:"question"`
	Propositions []PropBank `json:"propositions"; gorm:"foreignKey:QuestionRefer"`
	Response     string     `json:"response"`
	Anecdote     string     `json:"anecdote"`
}

/*
type Question struct {
	gorm.Model
	Question     string
	Propositions []PropBank `gorm:"foreignKey:QuestionRefer"`
	Response     string
	Anecdote     string
}
*/
type PropBank struct {
	gorm.Model
	Id            int `gorm:"primary_key, AUTO_INCREMENT"`
	prop          string
	QuestionRefer uint `gorm:"column:question_id"`
}

func main() {
	db, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	// db.DropTableIfExists(&PropBank{}, &Question{})
	db.AutoMigrate(&Question{}, &PropBank{})
	//db.Model(&PropBank{}).AddForeignKey("QuestionRefer", "Question(Id)", "CASCADE", "CASCADE")
	question1 := Question{Question: "This is good test",
		Response: "This is answer", Anecdote: "This is anecdote",
		Propositions: []PropBank{{"Ans 1"}, {"Ans 2"}}}
	db.Create(&question1)

}
