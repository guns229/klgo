package main

import (
	_ "github.com/mattn/go-sqlite3"
	"xorm.io/xorm"
)

const DB_FILE = "../db/faqs_xorm.db"

var engine *xorm.Engine

func check_err(err error) {
	if err != nil {
		panic(err)
	}
}

type InterviewSubjects struct {
	ID          int64  `xorm:"pk unique autoincr notnull"`
	SubjectName string `xorm:"varchar(30) unique notnull"`
}

type Topics struct {
	ID                  int64
	TopicName           string
	InterviewSubjectsID int64
}
type Questions struct {
	ID        int64
	Questions string
	TopicsID  int64
}

type Options struct {
	ID          int64
	Options     string
	Ans         bool
	QuestionsID int64
}

func main() {
	orm, err := xorm.NewEngine("sqlite3", DB_FILE)
	//mapper := core.GonicMapper{}

	//orm.SetMapper(mapper)

	defer orm.Close()
	orm.ShowSQL(true)
	check_err(err)
	// lets create tables
	err = orm.CreateTables(&InterviewSubjects{})
	check_err(err)
	err = orm.CreateTables(&Topics{})
	check_err(err)
	err = orm.CreateTables(&Questions{})
	check_err(err)
	err = orm.CreateTables(&Options{})
	check_err(err)
}
