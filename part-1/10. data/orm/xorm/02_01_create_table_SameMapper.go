package main

import (
	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"
	_ "github.com/mattn/go-sqlite3"
)

const DB_FILE = "db/faqs_xorm.db"

var engine *xorm.Engine

func check_err(err error) {
	if err != nil {
		panic(err)
	}
}

type InterviewSubjects struct {
	ID          int64  `xorm:"pk unique autoincr notnull"`
	SubjectName string `xorm:"varchar(30) unique notnull"`
}

func main() {
	orm, err := xorm.NewEngine("sqlite3", DB_FILE)
	orm.SetMapper(core.SameMapper{})
	defer orm.Close()
	orm.ShowSQL(true)
	check_err(err)
	// lets create tables
	err = orm.CreateTables(&InterviewSubjects{})
	check_err(err)
}
