package main

import "fmt"

func main() {
  // var A uint16 = 10 // 0b1010
  // var A int64 = 10 // 0b1010
  var A uint64 = 1 // 0b1010

  fmt.Println("10<<3:", A<<3) 
  fmt.Println("10<<33:", A<<33) 
  fmt.Println("10<<55:", A<<55) 
  fmt.Println("10<<56:", A<<56) 
  fmt.Println("10<<60:", A<<60) 
  fmt.Println("10<<63:", A<<63) 
  fmt.Println("10<<64:", A<<64) 
  fmt.Println("10<<65:", A<<65) 
}
