package main

import "fmt"

func main() {
	A := 'a'
	B := 'b'

	fmt.Println("A:", A, "B:", B)
	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
	fmt.Println("A > B: ", A > B)
	fmt.Println("A < B: ", A < B)
	fmt.Println("A >= B: ", A >= B)
	fmt.Println("A <= B: ", A <= B)
}
