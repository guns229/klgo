package main

import "fmt"

func main() {
	A := 10.19 // int
	B := 12.11 // float

	fmt.Println("A == B: ", A == B)
	fmt.Println("A != B: ", A != B)
	fmt.Println("A > B: ", A > B)
	fmt.Println("A < B: ", A < B)
	fmt.Println("A >= B: ", A >= B)
	fmt.Println("A <= B: ", A <= B)
}
