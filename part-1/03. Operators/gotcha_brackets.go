// gotcha_brackets.go 
package main

import "fmt"

func main() {
  var (
    // pi = 3.1416
    area = 314.16
  )
  // pi = a / r*r
  rad := float64(10)
  fmt.Println("Wrong answer", area / rad * rad )
  fmt.Println("Right Answer", area /(rad * rad ))
}
