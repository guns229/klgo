package main

import "fmt"

func main() {
  var (
    a = 10
    b = 10.3
    c =  3.141592653589793238
  )
  fmt.Println(a)
  a++
  fmt.Println(a)
  fmt.Println(b)
  b++
  fmt.Println(b)
  fmt.Println(c)
  c++
  fmt.Println(c)
  
}
