package main

import "fmt"

func main() {
	A := 1
	B := 33

	fmt.Println("A && B: ", A == B && B < A)
	fmt.Println("A || B: ", A == B || B > A)
	fmt.Println("!(A > 1)", !(A > 1))
}
