package main

import "fmt"

func main() {
	var (
		pointOne float32 = 0.000000001
		pointTwo float32 = 0.000000002
	)
	fmt.Println(0.000000001 + 0.000000002) // float64
	fmt.Println(pointOne + pointTwo)       // float32

	fmt.Println(0.1 + 0.2)

}
