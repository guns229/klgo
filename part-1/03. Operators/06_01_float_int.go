package main

import "fmt"

func main() {
	a := 10
	b := 12.2
	c := b + float64(a)
	fmt.Println(c)
	d := int64(b) + int64(a)
	fmt.Println(d)
	var e int8
	var f int16
	// Error Out
	// fmt.Println(e + f)
	fmt.Println(e + int8(f))
}
