package main

import (
	"fmt"
	"sync"
)

func countdown(title int) {
	defer wg.Done()

	for i := 0; i < 10; i++ {
		fmt.Println(title, "->", i)
	}
}

var wg sync.WaitGroup

func main() {
	wg.Add(10)
	for i := 0; i < 50; i++ {
		go countdown(i)
	}
	wg.Wait()

}
