package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {
	var increment int
	runtime.GOMAXPROCS(1)
	wg.Add(10)
	incre := func() {
		// time.Sleep(1 * time.Millisecond)
		increment++
		time.Sleep(1 * time.Millisecond)
		fmt.Println(increment)
	}

	// runtime.GOMAXPROCS(1)
	go incre()
	go incre()
	go incre()
	go incre()
	time.Sleep(1 * time.Millisecond)
	go incre()
	go incre()
	time.Sleep(1 * time.Millisecond)
	go incre()
	go incre()
	time.Sleep(1 * time.Second)
	fmt.Println("Done")

}
