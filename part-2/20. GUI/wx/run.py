#### IMPORT MODULES ####
import wx
import subprocess
import os
import wx.stc as stc
import keyword

#### FILE MENU ####
ID_F_NEW = wx.NewId()
ID_F_OPEN = wx.NewId()
ID_F_SAVE = wx.NewId()
ID_F_SAVEAS = wx.NewId()
ID_F_PREFS = wx.NewId()
ID_F_QUIT = wx.NewId()

#### EDIT MENU ####
ID_E_UNDO = wx.NewId()
ID_E_REDO = wx.NewId()
ID_E_CUT = wx.NewId()
ID_E_COPY = wx.NewId()
ID_E_PASTE = wx.NewId()
ID_E_SELECT = wx.NewId()
ID_E_FIND = wx.NewId()
ID_E_REPL = wx.NewId()

#### WXPYTHON MENU ####
ID_P_DEF = wx.NewId()
ID_P_PANEL = wx.NewId()
ID_P_MENUB = wx.NewId()
ID_P_TOOLB = wx.NewId()
ID_P_SCD = wx.NewId()
ID_P_MD = wx.NewId()
ID_P_TED = wx.NewId()
ID_P_STAT = wx.NewId()
ID_P_TXTBX = wx.NewId()
ID_P_BUTT = wx.NewId()
ID_P_SLID = wx.NewId()
ID_P_SPIN = wx.NewId()
ID_P_LIST = wx.NewId()
ID_P_CHEK = wx.NewId()
ID_P_IMPORT = wx.NewId()
ID_P_NEWID = wx.NewId()
ID_P_CLASS = wx.NewId()
ID_P_FUNC = wx.NewId()
ID_P_SIZR = wx.NewId()
ID_P_CON_S = wx.NewId()
ID_P_EVT = wx.NewId()
ID_P_BOIL = wx.NewId()
ID_P_DICT = wx.NewId()
ID_P_LIST = wx.NewId()
ID_P_TUPL = wx.NewId()
ID_P_DB = wx.NewId()

#### BUILD MENU ####
ID_B_RUN = wx.NewId()
ID_B_SAND = wx.NewId()
ID_B_COMP = wx.NewId()

#### HELP MENU ####
ID_H_ABOUT = wx.NewId()
ID_H_HELP = wx.NewId()

#### IDENTIFIER / VALUES ####
ID_P_DEF_CODE = ("import wx\n\n"
                "class nTmp(wx.Frame):\n"
                "   def __init__(self,parent,id):\n"
                "       wx.Frame.__init__(self,parent,id,'wxTemplate',size=(450,395))\n"
                "       panel = wx.Panel(self)\n"
                "       self.Centre()\n\n\n"

                "       #### CODE GOES HERE ####\n\n\n"

                "if __name__=='__main__':\n"
                "   app = wx.PySimpleApp()\n"
                "   frame = nTmp(parent=None,id=-1)\n"
                "   frame.Show()\n"
                "   app.MainLoop()\n")


#### CURRENT WXPYTHON FRAME ####
class Vodka(wx.Frame):
  def __init__(self,parent,id):

      #### Set window size to current display size, as well as compensating for task bar. ####
      WINDOW_SIZE = (wx.DisplaySize()[0]-20,wx.DisplaySize()[1]-60)

      wx.Frame.__init__(self,parent,id,"Vodka",size=(WINDOW_SIZE),pos=(30,100),style=(wx.BORDER_NONE))
      self.panel = wx.Panel(self)

      #### Completely centre the window. ####
      self.Centre()

      #### Create the Styled Text Control. ####
      self.text = stc.StyledTextCtrl(self.panel)
      self.text.SetCaretForeground("RED")
      self.text.StyleSetSpec(stc.STC_STYLE_LINENUMBER,  "fore:#C0C0C0,back:#000000")
      self.text.StyleSetSpec(stc.STC_STYLE_DEFAULT, "back:#000000,size:10,face:Courier New")
      self.text.SetWindowStyle(self.text.GetWindowStyle() | wx.NO_BORDER)
      self.text.SetWrapMode(stc.STC_WRAP_WORD)

      #### Create syntax highlighting for the STC. ####
      self.text.SetLexer(stc.STC_LEX_PYTHON)
      self.text.SetKeyWords(0, " ".join(keyword.kwlist))
      self.text.StyleSetSpec(wx.stc.STC_P_DEFAULT, 'fore:#FFFFFF,back:#000000')
      self.text.StyleSetSpec(wx.stc.STC_P_COMMENTLINE,  'fore:#00bff3,back:#000000')
      self.text.StyleSetSpec(wx.stc.STC_P_COMMENTBLOCK, 'fore:#00bff3,back:#000000')
      self.text.StyleSetSpec(wx.stc.STC_P_NUMBER, 'fore:#ec008c,back:#000000')
      self.text.StyleSetSpec(wx.stc.STC_P_STRING, 'fore:#ec008c,back:#000000')
      self.text.StyleSetSpec(wx.stc.STC_P_CHARACTER, 'fore:#ec008c,back:#000000')
      self.text.StyleSetSpec(wx.stc.STC_P_WORD, 'fore:#ec008c,back:#000000,bold')
      self.text.StyleSetSpec(wx.stc.STC_P_TRIPLE, 'fore:#ec008c,back:#000000')
      self.text.StyleSetSpec(wx.stc.STC_P_TRIPLEDOUBLE, 'fore:#ec008c,back:#000000')
      self.text.StyleSetSpec(wx.stc.STC_P_CLASSNAME, 'fore:#00ff00,back:#000000,bold')
      self.text.StyleSetSpec(wx.stc.STC_P_DEFNAME, 'fore:#00bff3,back:#000000,bold')
      self.text.StyleSetSpec(wx.stc.STC_P_OPERATOR, 'fore:#FFFFFF,back:#000000,bold')
      self.text.StyleSetSpec(wx.stc.STC_P_IDENTIFIER, 'fore:#00ff00,back:#000000')
      self.text.SetSelBackground(1, '#ffffff')

      #### Create the Wxpython Menu bar. ####
      m = wx.MenuBar()
      f = wx.Menu()
      f.Append(wx.ID_ANY,'New')
      f.Append(wx.ID_ANY,'Open...')
      f.AppendSeparator()
      f.Append(wx.ID_ANY,'Save')
      f.Append(wx.ID_ANY,'Save As...')
      f.AppendSeparator()
      f.Append(wx.ID_ANY,'Preferences...')
      f.AppendSeparator()
      f.Append(wx.ID_ANY,'Quit')
      ed = wx.Menu()
      ed.Append(wx.ID_ANY,'Undo')
      ed.Append(wx.ID_ANY,'Redo')
      ed.AppendSeparator()
      ed.Append(wx.ID_ANY,'Cut')
      ed.Append(wx.ID_ANY,'Copy')
      ed.Append(wx.ID_ANY,'Paste')
      ed.Append(wx.ID_ANY,'Select All')
      ed.AppendSeparator()
      ed.Append(wx.ID_ANY,'Find...')
      ed.Append(wx.ID_ANY,'Replace...')
      py = wx.Menu()
      g = wx.Menu()
      g.Append(ID_P_DEF,'Default')
      g.AppendSeparator()
      g.Append(wx.ID_ANY,'Panel')
      g.Append(wx.ID_ANY,'Menu Bar')
      g.Append(wx.ID_ANY,'Toolbar')
      d = wx.Menu()
      d.Append(wx.ID_ANY,'Single-Choice Dialog')
      d.Append(wx.ID_ANY,'Message Dialog')
      d.Append(wx.ID_ANY,'Text-Entry Dialog')
      g.AppendSeparator()
      g.AppendMenu(wx.ID_ANY, '&Dialog', d)
      g.AppendSeparator()
      g.Append(wx.ID_ANY,'Static Text')
      g.Append(wx.ID_ANY,'Text Box')
      g.Append(wx.ID_ANY,'Button')
      g.Append(wx.ID_ANY,'Slider')
      g.Append(wx.ID_ANY,'Spin Control')
      g.Append(wx.ID_ANY,'List Box')
      g.Append(wx.ID_ANY,'Check Box')
      py.AppendMenu(wx.ID_ANY, '&GUI', g)
      py.AppendSeparator()
      py.Append(wx.ID_ANY,'Import')
      py.Append(wx.ID_ANY,'ID')
      py.Append(wx.ID_ANY,'Class')
      py.Append(wx.ID_ANY,'Function')
      py.Append(wx.ID_ANY,'Sizer')
      py.Append(wx.ID_ANY,'Conditional Statement')
      py.Append(wx.ID_ANY,'Event')
      py.Append(wx.ID_ANY,'Boilerplate')
      py.AppendSeparator()
      py.Append(wx.ID_ANY,'Dictionary')
      py.Append(wx.ID_ANY,'List')
      py.Append(wx.ID_ANY,'Tuple')
      py.AppendSeparator()
      py.Append(wx.ID_ANY,'Database Connectivity...')
      b = wx.Menu()
      b.Append(ID_B_RUN,'Run')
      b.Append(wx.ID_ANY,'Sandbox')
      b.Append(wx.ID_ANY,'Compile')
      h = wx.Menu()
      h.Append(wx.ID_ANY,'About Vodka')
      h.AppendSeparator()
      h.Append(wx.ID_ANY,'Vodka Help')
      m.Append(f,'&File')
      m.Append(ed,'&Edit')
      m.Append(py,'&Python')
      m.Append(b,'&Build')
      m.Append(h,'&Help')
      #self.SetMenuBar(m)


      #### Add the STC to a Sizer. ####
      sizer = wx.BoxSizer(wx.VERTICAL)
      sizer.Add(self.text, 1, wx.EXPAND,border=20)
      self.panel.SetSizer(sizer)

      sizer = wx.BoxSizer(wx.VERTICAL)
      sizer.Add(self.panel, 1, wx.EXPAND)
      self.SetSizer(sizer)

      self.Layout()




      #### EVENT BINDINGS ###############################

      #### Add line numbers to the STC upon entering. ###
      self.text.Bind(stc.EVT_STC_CHANGE, self.OnChange)
      self.Show()

      #### Add Default wxpython GUI code to the STC. ####
      self.Bind(wx.EVT_MENU,self.DEFAULT_GUI, id=ID_P_DEF)

      #### Select 'Run' from the Build Menu. ####
      self.Bind(wx.EVT_MENU,self.RUN_CODE, id=ID_B_RUN)


  #### EVENT FUNCTIONS ####

  #### Add line numbers to the STC upon entering. ###
  def OnChange(self, e):
      lines = self.text.GetLineCount()
      width = self.text.TextWidth(stc.STC_STYLE_LINENUMBER, " "+str(lines)+" ")
      self.text.SetMarginWidth(0, width)
      self.text.SetMarginLeft(10)
      self.text.SetIndent(2)

  #### Add Default wxpython GUI code to the STC. ####
  def DEFAULT_GUI(self, e):
      self.text.AddText(ID_P_DEF_CODE)

  #### Select 'Run' from the Build Menu. ####
  def RUN_CODE(self, e):
    pass

#### BOILERPLATE ####
if __name__=='__main__':
  app = wx.App()
  Vodka_Frame = Vodka(None,-1)
  Vodka_Frame.Show()
  Vodka_Frame.SetTransparent(230)
  app.MainLoop()
