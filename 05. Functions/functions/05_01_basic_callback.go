package main

import "fmt"

func magic_me(x int, callback func(n int)) {
	fmt.Println(x)

	if x <= 20 {
		callback(x)
	} else {
		callback(x / 2)
	}
}

func main() {

	magic_me(10, func(n int) {
		fmt.Println(n * n)
	})
	
  magic_me(100, func(n int) {
		fmt.Println(n * n)
	})
}
