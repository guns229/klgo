package main

import (
	"log"
	"strconv"
	"time"
)

func timeIt(func_name string, startTime time.Time) {
	timeTaken := time.Since(startTime).Nanoseconds()
	log.Printf("%s took time to execute: %d", func_name, timeTaken)
}

func seq(x int) int {
	total := 1
	if x == 0 {
		return total
	} else {
		// if x < 0 {
		// 	x = -x
		// }
		for i := 1; i < x; i++ {
			total = total + i
		}
		return total
	}
}

func seq_test(x int) {
	defer timeIt("for "+strconv.Itoa(x), time.Now())
	seq(x)
}

func main() {
	for x := 1; x < 100000; x = x + 1000 {
		seq_test(x)
	}
}
