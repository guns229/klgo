package main

import "fmt"

func LetsAdd(initial int, remaining ...int) {
  total := initial
  
  for i := 0; len(remaining) >= (i + 1); i++ {
    fmt.Println(i)
    total += remaining[i]
  }
  
  fmt.Println(total, len(remaining))
}

func main() {
  LetsAdd(1, 0)
  LetsAdd(0, 12, 2, 1, 4)
}
