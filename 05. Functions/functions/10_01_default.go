package main

import "fmt"

func test(a int, b int) {
  // we are assuming `0` is not a logical option for the values of `a`
  if a == 0 {
    a = 10  // Assigning default value
  }
  if b == 0 {
    b = 10  // Assigning default value
  }
  fmt.Println(a, b)
}

func main() {
  test(1, 0)
  test(0, 12)
}
