package main

import "fmt"

func AddAll(vals ...int) int{
  total := 0
  for _, val := range vals {
    total += val
  }
  return total
}

func main() {
	fmt.Println(AddAll(1, 2, 4, 2))
	fmt.Println(AddAll(1 ))
	fmt.Println(AddAll(4, 2))
}
