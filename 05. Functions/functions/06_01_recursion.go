package main

import "fmt"

func seq(x int64) int64 {
	if x != 0 {
		return x * seq(x-1)
	} 
	return 1
}

func main() {
  var i int64
	for i = 1; i < 10; i++ {
		fmt.Println(seq(i))
	}

}
