package main

import "fmt"

type MyError struct {
	error_code int
	msg        string
}

func (me *MyError) Init(err_code int) {
	me.error_code = err_code
	switch err_code {
	case 0:
		me.msg = "No error"
	case 101:
		{
			me.msg = "Too low value"
		}
	case 201:
		{
			me.msg = "Too high value"
		}
	default:
		{
			me.msg = "unknown error"
		}
	}
}

func (me MyError) GetMsg() string {
	return me.msg
}

func custom_sum(x int16, y int16) (int16, *MyError) {
	me := new(MyError)
	switch {
	case x <= 32760:
		{
			fmt.Println("adding")
			me.Init(0)
			return x + y, me
		}
	case x > 32760:
		{
			me.Init(201)
			return 0, me
		}
	case x < 5:
		{
			me.Init(101)
			return 0, me
		}
	}
	return 0, me
}

func main() {
	val, err := custom_sum(10, 11)
	fmt.Println(val, err.GetMsg())
	val, err = custom_sum(32761, 11)
	fmt.Println(val, err.GetMsg())
}
