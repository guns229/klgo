package main

import (
	"log"
	"strconv"
	"time"
)

func timeIt(func_name string, startTime time.Time) {
	// ** Why startTime is not inside the function **
	//   Because we are going to use defer and thus startTime
	//   can not be inside the function, but supplied from
	//   outside the function :)
	timeTaken := time.Since(startTime).Nanoseconds()
	log.Printf("%s took time to execute: %d", func_name, timeTaken)
}

func seq(x int) int {

	if x != 0 {
		return x + seq(x-1)
	} else {
		return 1
	}
}

func seq_test(x int) {
	defer timeIt("for "+strconv.Itoa(x), time.Now())
	seq(x)
}

func main() {
	for x := 1; x < 100000; x = x + 1000 {
		seq_test(x)
	}
}
