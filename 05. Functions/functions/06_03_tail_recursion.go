package main

import "fmt"

func seq(x int, total int) int {
	if x != 0 {
		return seq(x-1, x*total)
	} else {
		return total
	}
}

func main() {
	for i := 1; i < 10; i++ {
		fmt.Println(seq(i, 1))
	}
}
