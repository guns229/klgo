# Controls
- - -
[TOC]

## Introduction
Controls are the basic building blocks of any programming language.

## `if`

The basis structure of `if` loop is similar to `c/c++` .  

### Syntax

```go
if <condition>{
    <code block>
}
```





## `for`

The basis structure of `for` loop is similar to `c/c++` .  

### `Syntax`

```go
for <loop condition>{
    loop
    <break>
    <continue>
}
```

