package main

import "fmt"

type Users struct {
	Name string
	Id   int
}

type Computers struct {
	Hostname string
	Id       int
}

func main() {
	// mayank := Users{"Wei Wu", 1001}
	laptopV1 := Computers{"Wei Wu", 1001}
	laptopV2 := Computers{"Lab 1", 1002}
	laptopV3 := Computers{"Lab 1", 1002}

	// fmt.Println(laptopV1 == mayank)
	// fmt.Println(laptopV2 == mayank)
	fmt.Println(laptopV1 == laptopV2)
	fmt.Println(laptopV3 == laptopV2)

}
