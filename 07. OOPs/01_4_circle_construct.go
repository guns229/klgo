package main

import (
	"fmt"
	"math"
)

const (
	PI float64 = 3.141592653589
)

type Circle struct {
	radius float64
	area   float64
}

func NewCircleRadius(radius float64) *Circle {
	c := new(Circle)
	c.SetRadius(radius)
	return c
}

func NewCircleArea(area float64) *Circle {
	c := new(Circle)
	c.SetArea(area)
	return c
}

// Getter Function
func (c Circle) Radius() float64 {
	return c.radius
}

// Setter Function
func (c *Circle) SetRadius(val float64) {
	c.radius = val
	c.area = PI * val * val
}

// Getter Function
func (c Circle) Area() float64 {
	return c.area
}

// Setter Function
func (c *Circle) SetArea(val float64) {
	c.area = val
	c.radius = math.Sqrt(val / PI)
}

func main() {
	// c := new(Circle)
	c := NewCircleRadius(12)
	fmt.Println(c.Radius(), c.Area())
	// Area will not update.
	c.radius = 20
	fmt.Println(c.Radius(), c.Area())
	newCircle := NewCircleArea(100)
	fmt.Println(newCircle.Radius(), newCircle.Area())
}
