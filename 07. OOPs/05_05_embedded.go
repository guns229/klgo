package main

import (
	"fmt"
	"strings"
)

// Parent 1
type Users struct {
	Name string
	Id   int32
}

func (u Users) SetName(name string) {
	fmt.Println("In Users SetName")
	u.Name = strings.TrimSpace(name)
}

// Parent 2
type Technical struct {
	TechName string
	Rating   int8
}

func (t Technical) SetTechName(techName string) {
	t.TechName = techName
}

// Parent 3
type Managers struct {
	Reportee bool

	Name string
}

func (u Managers) SetName(name string) {
	fmt.Println("In Managers SetName")
	u.Name = strings.TrimSpace(name)
}

type TechManagers struct {
	Users
	Technical
	Managers
}

func main() {
	var mayank TechManagers
	mayank.Id = 10001
	mayank.Name = "Testing"
	mayank.Reportee = true
	fmt.Println(mayank)
	mayank.SetName("MJ")
	fmt.Println(mayank)
}
