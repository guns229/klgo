package main

import "fmt"

type A interface {
	B
}

type B interface {
	C
}

type C interface {
	A
}

func main() {
	var a A
	fmt.Println(a)
}
