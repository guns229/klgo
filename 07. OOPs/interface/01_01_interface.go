package main

import (
	"fmt"
	"regexp"
)

// we have an interface books, which can used by
// school books, novels, etc
type Books interface {
	GetWordCount() int
	GetAuthor() string
}

type School_Book struct {
	author string
	pages  string
}

func get_words_from(text string) []string {
	words := regexp.MustCompile(`\w+`)
	return words.FindAllString(text, -1)
}

func (sb School_Book) GetWordCount() int {
	return len(get_words_from(sb.pages))
}

func (sb School_Book) GetAuthor() string {
	return "Author is " + sb.author
}

func describe(i Books) {
	fmt.Printf("(%v, %T)\n", i, i)
}

func main() {
	var dms Books
	dms = School_Book{
		"Mayank Johri",
		"This is my first maths book, dot dot dot",
	}

	fmt.Println(dms.GetWordCount())
}
