package main

import "fmt"

type dummy interface{}

type User struct {
	Id int
}
type Managers struct {
	Id int
}

func DisplayId(d dummy) {
	fmt.Println(d)
}

func main() {
	var u dummy
	var m dummy
	u = User{10}
	m = Managers{100}
	DisplayId(u)
	DisplayId(m)
	DisplayId(100)
}
