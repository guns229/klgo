package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

type Colr struct {
	Name string `json: "Name"`
	Colr string `json: "colr"`
}

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	}
}

func toJson(p interface{}) string {
	bytes, err := json.Marshal(p)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return string(bytes)
}

func getThemes() []Colr {
	raw, err := ioutil.ReadFile("./theme.json")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	var c []Colr
	json.Unmarshal(raw, &c)
	return c
}

func main() {
	colr := getThemes()
	for _, p := range colr {
		fmt.Println(p)
	}

	fmt.Println(toJson(colr))
}
