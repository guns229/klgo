package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"github.com/dontpanic92/wxGo/wx"
)

type MyEditor struct {
	wx.StyledTextCtrl
}

type MyFrame struct {
	wx.Frame
	auiManager wx.AuiManager
	menuBar    wx.MenuBar
	self       wx.Frame
	Editor     MyEditor
}

const (
	ID_Settings = iota + wx.ID_HIGHEST + 1
	ID_SampleItem
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	}
}

type Colr struct {
	Name string `json: "Name"`
	Colr string `json: "colr"`
}

func toJson(p interface{}) string {
	bytes, err := json.Marshal(p)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return string(bytes)
}

func getThemes() []Colr {
	raw, err := ioutil.ReadFile("./theme.json")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	var c []Colr
	json.Unmarshal(raw, &c)
	return c
}

func apply_theme(theme Colr, editor wx.StyledTextCtrl) {
	name := 0
	switch theme.Name {
	case "wx.STC_P_DEFAULT":
		name = wx.STC_P_DEFAULT
	case "wx.STC_P_COMMENTLINE":
		name = wx.STC_P_COMMENTLINE
	case "wx.STC_P_COMMENTBLOCK":
		name = wx.STC_P_COMMENTBLOCK
	case "wx.STC_P_NUMBER":
		name = wx.STC_P_NUMBER
	case "wx.STC_P_STRING":
		name = wx.STC_P_STRING
	case "wx.STC_P_CHARACTER":
		name = wx.STC_P_CHARACTER
	case "wx.STC_P_WORD":
		name = wx.STC_P_WORD
	case "wx.STC_P_TRIPLEDOUBLE":
		name = wx.STC_P_TRIPLEDOUBLE
	case "wx.STC_P_TRIPLE":
		name = wx.STC_P_TRIPLE
	case "wx.STC_P_CLASSNAME":
		name = wx.STC_P_CLASSNAME
	case "wx.STC_P_DEFNAME":
		name = wx.STC_P_DEFNAME
	case "wx.STC_P_OPERATOR":
		name = wx.STC_P_OPERATOR
	case "wx.STC_P_IDENTIFIER":
		name = wx.STC_P_IDENTIFIER
	}
	editor.StyleSetSpec(name, theme.Colr)
}

func NewMyFrame() *MyFrame {
	keywords := []string{"False",
		"None",
		"True",
		"and",
		"as",
		"assert",
		"break",
		"class",
		"continue",
		"def",
		"del",
		"elif",
		"else",
		"except",
		"finally",
		"for",
		"from",
		"global",
		"if",
		"import",
		"in",
		"is",
		"lambda",
		"nonlocal",
		"not",
		"or",
		"pass",
		"raise",
		"return",
		"try",
		"while",
		"with",
		"yield"}

	self := &MyFrame{}
	self.Frame = wx.NewFrame(wx.NullWindow, wx.ID_ANY, "Maa IDE")

	// AUI Init

	self.auiManager = wx.NewAuiManager()
	self.auiManager.SetManagedWindow(self)

	// We should call auiManager.UnInit() in frame's destructor, but unfortunately
	// we can't get a callback when frame's destructor called. So we do this in the
	// EVT_CLOSE_WINDOW. And also because we listen the CloseEvent, we have to call
	// Destroy() manually.
	wx.Bind(self, wx.EVT_CLOSE_WINDOW, func(e wx.Event) {
		self.auiManager.UnInit()
		self.Destroy()
	}, self.GetId())

	// Menu

	self.menuBar = wx.NewMenuBar()
	fileMenu := wx.NewMenu()
	fileMenu.Append(wx.ID_EXIT)

	wx.Bind(self, wx.EVT_MENU, func(e wx.Event) {
		self.Close(true)
	}, wx.ID_EXIT)

	self.menuBar.Append(fileMenu, "&File")
	self.SetMenuBar(self.menuBar)

	// StatusBar

	self.CreateStatusBar()
	self.GetStatusBar().SetStatusText("Ready")

	// AuiToolBar

	// toolBar1 := wx.NewAuiToolBar(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.AUI_TB_DEFAULT_STYLE|wx.AUI_TB_OVERFLOW)
	// toolBar1.SetToolBitmapSize(wx.NewSizeT(48, 48))

	// toolBar1.AddTool(ID_SampleItem+1, "Test", wx.ArtProviderGetBitmap(wx.ART_ERROR))
	// toolBar1.AddSeparator()
	// toolBar1.AddTool(ID_SampleItem+2, "Test", wx.ArtProviderGetBitmap(wx.ART_QUESTION))
	// toolBar1.AddTool(ID_SampleItem+3, "Test", wx.ArtProviderGetBitmap(wx.ART_INFORMATION))
	// toolBar1.AddTool(ID_SampleItem+4, "Test", wx.ArtProviderGetBitmap(wx.ART_WARNING))
	// toolBar1.AddTool(ID_SampleItem+5, "Test", wx.ArtProviderGetBitmap(wx.ART_MISSING_IMAGE))

	// separator := wx.NewAuiToolBarItemT()
	// separator.SetKind(wx.ITEM_SEPARATOR)
	// customButton := wx.NewAuiToolBarItemT()
	// customButton.SetKind(wx.ITEM_NORMAL)
	// customButton.SetLabel("Customize...")
	// wx.Bind(self, wx.EVT_MENU, func(e wx.Event) {
	// 	wx.MessageBox("Customize clicked!")
	// }, customButton.GetId())

	// toolBar1.SetCustomOverflowItems([]wx.AuiToolBarItem{}, []wx.AuiToolBarItem{separator, customButton})
	// toolBar1.Realize()

	// paneInfo := wx.NewAuiPaneInfoT().Name("tb1").Caption("Big Toolbar").ToolbarPane().Top()
	// self.auiManager.AddPane(toolBar1, paneInfo)

	// Left pane - a tree control

	treeCtrl := wx.NewTreeCtrl(self, wx.ID_ANY, wx.DefaultPosition,
		wx.NewSizeT(160, 250),
		wx.TR_DEFAULT_STYLE|wx.NO_BORDER)

	treeRoot := treeCtrl.AddRoot("wxAUI Project", 0)

	firstItem := treeCtrl.AppendItem(treeRoot, "Item 1", 0)
	treeCtrl.AppendItem(treeRoot, "Item 2", 0)
	treeCtrl.AppendItem(treeRoot, "Item 3", 0)
	treeCtrl.AppendItem(treeRoot, "Item 4", 0)
	treeCtrl.AppendItem(treeRoot, "Item 5", 0)
	treeCtrl.AppendItem(firstItem, "Subitem 1", 1)
	treeCtrl.AppendItem(firstItem, "Subitem 2", 1)
	treeCtrl.AppendItem(firstItem, "Subitem 3", 1)
	treeCtrl.AppendItem(firstItem, "Subitem 4", 1)
	treeCtrl.AppendItem(firstItem, "Subitem 5", 1)
	treeCtrl.Expand(treeRoot)

	self.auiManager.AddPane(wx.ToWindow(treeCtrl), wx.LEFT, "Hello")

	paneInfo := wx.NewAuiPaneInfoT().Name("notebook").CenterPane().PaneBorder(false)
	notebook := wx.NewAuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.AUI_NB_DEFAULT_STYLE|wx.AUI_NB_CLOSE_BUTTON)
	Editor := wx.NewStyledTextCtrl(notebook)
	Editor.CallTipActive()

	Editor.SetTabIndents(false)
	Editor.SetTabWidth(4)
	Editor.SetAutomaticFold(1)
	Editor.SetHelpText("TEST")
	Editor.SetLexer(wx.STC_LEX_PYTHON)
	Editor.SetStyleBits(Editor.GetStyleBitsNeeded())
	Editor.SetThemeEnabled(true)
	Editor.SetAutoLayout(true)
	Editor.SetHint("true")
	Editor.AutoCompleteFileNames()
	Editor.SetProperty("fold", "1")
	Editor.SetProperty("tab.timmy.whinge.level", "1")
	Editor.SetMargins(0, 0)
	Editor.SetViewWhiteSpace(1)
	Editor.SetMarginType(1, wx.STC_MARGIN_SYMBOL|wx.STC_MARGIN_NUMBER)
	Editor.SetMarginMask(2, wx.STC_MASK_FOLDERS)
	Editor.SetMarginSensitive(2, true)
	Editor.SetMarginWidth(2, 25)
	Editor.SetEdgeColumn(78)
	Editor.UsePopUp(false)
	Editor.SetUseTabs(false)
	Editor.SetTabWidth(4)
	Editor.SetAutoLayout(true)
	Editor.AutoCompStops(" .,;:([)]}\\'\"\\<>%^&+-=*/|`")
	Editor.MarkerDefine(wx.STC_MARKNUM_FOLDEROPEN, wx.STC_MARK_BOXMINUS, wx.NewColour("white"), wx.NewColour("black"))
	Editor.MarkerDefine(wx.STC_MARKNUM_FOLDER, wx.STC_MARK_BOXPLUS, wx.NewColour("white"), wx.NewColour("black"))
	Editor.MarkerDefine(wx.STC_MARKNUM_FOLDERSUB, wx.STC_MARK_VLINE, wx.NewColour("white"), wx.NewColour("black"))
	Editor.MarkerDefine(wx.STC_MARKNUM_FOLDERTAIL, wx.STC_MARK_LCORNER, wx.NewColour("white"), wx.NewColour("black"))
	Editor.MarkerDefine(wx.STC_MARKNUM_FOLDEREND, wx.STC_MARK_BOXPLUSCONNECTED, wx.NewColour("white"), wx.NewColour("black"))
	Editor.MarkerDefine(wx.STC_MARKNUM_FOLDEROPENMID, wx.STC_MARK_BOXMINUSCONNECTED, wx.NewColour("white"), wx.NewColour("black"))
	Editor.MarkerDefine(wx.STC_MARKNUM_FOLDERMIDTAIL, wx.STC_MARK_TCORNER, wx.NewColour("white"), wx.NewColour("black"))
	// Editor.AutoCompComplete()

	// Editor.PopupMenu(keywords)
	// Editor.SetAuto(keywords)
	wx.Bind(self, wx.EVT_STC_MARGINCLICK, self.evtMarginClick, self.GetId())
	wx.Bind(notebook, wx.EVT_STC_CHARADDED, self.evntKeyDown, self.GetId())
	// wx.Bind(Editor)
	key_string := strings.Join(keywords, " ")
	fmt.Println()
	Editor.SetKeyWords(0, key_string)
	// Editor.AutoCompShow(0, key_string)
	notebook.AddPage(Editor, "Test", true)
	self.auiManager.AddPane(notebook, paneInfo)

	colr := getThemes()
	// fmt.Println("...")
	// fmt.Println(colr)
	for _, p := range colr {
		apply_theme(p, Editor)
		// Editor.StyleSetForeground(p.Name, wx.NewColour(uint(p.Col)))
		// fmt.Println(p)
	}
	u, _ := strconv.ParseUint("0x000000", 0, 32)
	d := wx.NewColour(uint(u))
	Editor.SetSelBackground(true, d)
	Editor.SetBackgroundColour(d)
	Editor.StyleSetSpec(wx.STC_STYLE_DEFAULT, "back:#000000,size:10,face:Courier New")
	d.SetRGBA(uint(0xffffff))
	Editor.SetCaretForeground(wx.SwigIsColour(d))

	self.auiManager.Update()
	wx.Bind(self, wx.EVT_STC_CHARADDED, func(e wx.Event) {
		fmt.Println("TEST")

	}, Editor.GetId())

	return self
}

func (b *MyFrame) evntKeyDown(evt wx.Event) {
	fmt.Println("key evntKeyDown")

}

func (f *MyFrame) evtKeyDown(evt wx.Event) {
	fmt.Println("key downb")
}

func (f *MyFrame) evtMarginClick(evt wx.Event) {
	fmt.Println(evt)

	f.FoldAll()
}

func (f *MyFrame) FoldAll() {
	lineCount := f.Editor.GetLineCount()
	fmt.Println(lineCount)
}

func main() {
	wx1 := wx.NewApp()
	frame := NewMyFrame()
	frame.Show()
	wx1.MainLoop()
	return
}
