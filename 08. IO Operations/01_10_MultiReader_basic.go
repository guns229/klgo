package main

import (
	"fmt"
	"io"
	"strings"
)

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	} else if err == io.EOF {
		fmt.Println("thanks, file ended", err)
	}
}

func main() {
	r_header := strings.NewReader(`YO Reader,
		`)
	r_body := strings.NewReader(`
		How are you doing,
		I had good time in Aligarh.
		`)
	r_tail := strings.NewReader(`Bye,
		Mayank J.
		`)

	// make a buffer to keep chunks that are read

	buf := make([]byte, 60)

	mr := io.MultiReader(r_header, r_body, r_tail)
	for {
		count, err := mr.Read(buf)
		check(err)
		if count == 0 {
			break
		}
		fmt.Printf("len = %d, data = %q\n", count, buf[:count])
	}
}
