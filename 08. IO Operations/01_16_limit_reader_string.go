package main

import (
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	r := strings.NewReader(`ॐ भूर्भुवः स्वः तत्सवितुर्वरेण्यं
भर्गो देवस्यः धीमहि धियो यो नः प्रचोदयात्॥`)
	lr := io.LimitReader(r, 3)

	if _, err := io.Copy(os.Stdout, lr); err != nil {
		log.Fatal(err)
	}
}
