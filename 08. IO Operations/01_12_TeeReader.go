package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	r := strings.NewReader(`ॐ भूर्भुवः स्वः तत्सवितुर्वरेण्यं
भर्गो देवस्यः धीमहि धियो यो नः प्रचोदयात्॥`)
	var buf bytes.Buffer
	tee := io.TeeReader(r, &buf)
	read_1, _ := ioutil.ReadAll(tee)
	read_2, _ := ioutil.ReadAll(&buf)
	log.Printf("%s", read_1)
	log.Printf("%s", read_2)
}
