package main

import (
	"fmt"
	"io"
	"strings"
)

func main() {
	// nepali for traveling on train
	r := strings.NewReader("रेलमा यात्रा गर्दै")
	// **PLEASE**
	// do not change byte size from 50
	buf := make([]byte, 50)
	for {
		if _, err := io.ReadFull(r, buf); err == io.EOF || err != nil {
			break
		} else {
			fmt.Printf("b = %q\n", buf)
		}
	}
}
