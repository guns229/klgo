package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"strings"
)

func tt(a ...interface{}) []interface{} {
	return a
}

func main() {
	// NOT WORKING
	r := strings.NewReader(`ॐ भूर्भुवः स्वः तत्सवितुर्वरेण्यं
भर्गो देवस्यः धीमहि धियो यो नः प्रचोदयात्॥`)
	var buf bytes.Buffer
	tee := io.TeeReader(r, &buf)
	// read_1, _ := ioutil.ReadAll(tee)
	read_2, _ := ioutil.ReadAll(&buf)
	log.Printf("%s", tt(ioutil.ReadAll(tee)[0]))
	log.Printf("%s", read_2)
}
