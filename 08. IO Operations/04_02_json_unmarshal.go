package main

import (
	"encoding/json"
	"fmt"
	"io"
)

type Questions struct {
	ID       float64
	Question string
	Live     bool
}

func check(err error) {
	if err != nil && err != io.EOF {
		fmt.Print(err)
		panic(err)
	}
}

func main() {
	questions := `[{"ID":0,
					"Question":"How do I list all files of a directory?",
					"Live":true},
				{"ID":0.1,
					"Question":"How do I parse values from a JSON file?",
					"Live":false}]`
	var quests interface{}
	err := json.Unmarshal([]byte(questions), &quests)
	check(err)
	fmt.Println(quests)
}
