package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello " + "World")
	var val = "String"
	fmt.Println("Testing with " + val)
	fmt.Println("Sum of 1 + 1 = ", 1+1)
	fmt.Println("Multiplication of 1.22 & 23 = ", 1.22*23)
	fmt.Println("`true && false` is ", true && false)
	fmt.Println("true || false = ", true || false)
	fmt.Println("1 > 2 = ", 1 > 2)
	fmt.Println("!true = ", !true)

	var myString string
	fmt.Println(">" + myString + "<")
	var myCount int
	fmt.Println(myCount)
	fmt.Println("------------")
}
