package main

import "fmt"

func main() {
	var (
		myCount int
		name    = "Mayank Johri"
		flg     bool
		x, y, z = 1, "test", true
	)
	fmt.Println(x, y, z)
	fmt.Println(myCount, name, flg)
}
